package us.solute.sensorservice.stream.impl

import akka.{Done, NotUsed}
import akka.stream.scaladsl.{Flow, Source}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import us.solute.sensorservice.stream.api.SensorServiceStream

import scala.concurrent.Future
import com.lightbend.lagom.scaladsl.pubsub.PubSubRegistry
import com.lightbend.lagom.scaladsl.pubsub.TopicId
import us.solute.sensor.service.api.{AOUUpdate, SensorService}

/**
  * Implementation of the SensorServiceStreamService.
  */
class SensorServiceStreamImpl(sensorService: SensorService, pubSub: PubSubRegistry) extends SensorServiceStream {

  val topic = pubSub.refFor(TopicId[AOUUpdate]("sensorTopic"))

  sensorService.aouTopic().subscribe.atLeastOnce(
    Flow.fromFunction(aouUpdate => {
      topic.publish(aouUpdate)
      Done
    }))

  def sensorStream = ServiceCall { _ => {
    val topic = pubSub.refFor(TopicId[AOUUpdate]("sensorTopic"))
    Future.successful(topic.subscriber)
  }
  }

}

