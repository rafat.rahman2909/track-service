package us.solute.sensorservice.stream.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.trackservice.api.{Position, TrackService, TrackUpdate}
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaClientComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.pubsub.PubSubComponents
import com.softwaremill.macwire._
import us.solute.sensor.service.api.{AOUUpdate, SensorService}
import us.solute.sensorservice.stream.api.SensorServiceStream

import scala.collection.immutable.Seq

class SensorServiceStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new SensorServiceStreamApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new SensorServiceStreamApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[SensorServiceStream])
}

abstract class SensorServiceStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with LagomKafkaClientComponents
    with ClusterComponents
    with PubSubComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[SensorServiceStream](wire[SensorServiceStreamImpl])

  override lazy val jsonSerializerRegistry = SensorServiceStreamSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]

  // Bind the SensorService client
  lazy val sensorService: SensorService = serviceClient.implement[SensorService]
}


object SensorServiceStreamSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[AOUUpdate]
  )
}