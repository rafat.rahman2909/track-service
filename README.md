# Track Service

TODO: One Paragraph of project description goes here

## Getting Started

This project relies on Java and sbt to manage dependencies and run the services locally. The application can be run using the following command:
```
sbt runAll
```
With the code running you can test that the application is running correctly by sending the following curl command:
``` 
curl -H "Content-Type: application/json" -X POST -d '{ "id":"4","code":"code","position":{"latitude":67.498454,"longitude":21.040181,"alt":"alt"} }' http://localhost:9000/api/tracks
```
Navigate [HERE](http://localhost:9000/api/tracks) and verify that you see the following JSON
``` 
{
    "id": "4",
    "code": "code",
    "position": {
      "alt": "alt",
      "latitude": 67.49845123291016,
      "longitude": 21.040180206298828
    }
}
```

During development use the following to reflect new changes in your local environment
:
``` 
sbt clean cleanFiles runAll
```
sbt can be run in debug mode to allow intellij to use breakpoints on port 5005 (or any specified port).
You can run the app in debug mode with the following
``` 
sbt -jvm-debug 5005
==> then after you should see 
sbt:track-service>
```
run the following to run the server in debug mode
``` 
clean
cleanFiles
runAll
```
### Prerequisites

The following are needed to run this application locally

```
Required:
    Java JDK >= 1.8
    sbt == 1.3.2 (issues with newer versions Unrecognized option: -J-Xms512M)
```

### Installing

TODO: A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Tests can be run using the following command from project root
```
sbt test
```

### Break down into end to end tests

TODO: Explain what these tests test and why

```
Give an example
```

### Coding style

We will be using the official [Scala style guide](https://docs.scala-lang.org/style/). Please refer to the documentation for specific examples 

## Deployment

TODO: Add additional notes about how to deploy this on a live system

## Built With
TODO: include brief sentence of what each piece is doing
* [Java](https://www.java.com/en/) - coffee
* [Scala](https://www.scala-lang.org/) - High Level Language
* [sbt](https://www.scala-sbt.org/) - Dependency Management and Build Tool
* [Akka](https://akka.io/) - Tool for Concurrent and Distributed Applications
* [Cassandra](http://cassandra.apache.org/) - NoSQL Database
* [Lagom](https://www.lagomframework.com/) - Microservices Framework

## Contributing

Please visit [MST Onboarding](https://confluence.apps.solute.us/display/MST/MST+Onboarding) for details on our code of conduct and the process for submitting pull request.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

TODO: include info on our license

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* Tobey Maguire for being the best Spider-man
* etc