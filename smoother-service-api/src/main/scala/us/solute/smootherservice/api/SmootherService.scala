package us.solute.smootherservice.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}
import us.solute.trackservice.api.{Cart, Position}
import us.solute.sensor.service.api.{AOUUpdate, Ellipse, UserInput}

object SmootherService  {
  val TOPIC_NAME = "smoothedAOU"
}

trait SmootherService extends Service {

  /**
    * Example: curl http://localhost:9000/api/smoothedaou/0
    */
  def smoothedAou(id: String): ServiceCall[NotUsed, SmoothedAOU]


  /**
   * Example: curl http://localhost:9000/api/aou
   */
  def smoothedAous(): ServiceCall[NotUsed, List[SmoothedAOU]]


  /**
    * Example: curl http://localhost:9000/api/smoothedaou
    */
  def updateSmoothedAOU(): ServiceCall[SmoothedAOU, Done]

  /**
    * Example: curl http://localhost:9000/api/smoothedaou/start
    */
  def startSmoothedAOU(): ServiceCall[SmootherInput, Done]

  /**
    * This gets published to Kafka.
    */
  def smoothedTopic(): Topic[SmoothedAOU]


  override final def descriptor: Descriptor = {
    import Service._
    // @formatter:off
    named("smoother-service")
      .withCalls(
        pathCall("/api/smoothedaou/:id", smoothedAou _),
        pathCall("/api/smoothedaou", updateSmoothedAOU _),
        pathCall("/api/smoothedaou", smoothedAous _),
        pathCall("/api/smoothedaou/start", startSmoothedAOU _)
      )
      .withTopics(
        topic(SmootherService.TOPIC_NAME, smoothedTopic _)
      )
      .withAutoAcl(true)
    // @formatter:on
  }

}

case class SmoothedAOU(id: String, centerPt: Position, ellipse: Ellipse, trackLat: Double, trackLong: Double, time: String)
object SmoothedAOU {
  implicit val format: Format[SmoothedAOU] = Json.format[SmoothedAOU]
}

case class SmootherInput(tracksPerIteration: Double)
object SmootherInput {
  implicit val format: Format[SmootherInput] = Json.format[SmootherInput]
}
