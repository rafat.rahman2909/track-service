// The Lagom plugin
addSbtPlugin("com.lightbend.lagom" % "lagom-sbt-plugin" % "1.5.3")

addSbtPlugin("com.dwijnand" % "sbt-dynver" % "3.3.0")