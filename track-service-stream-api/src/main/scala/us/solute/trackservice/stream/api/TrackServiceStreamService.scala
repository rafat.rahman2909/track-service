package us.solute.trackservice.stream.api

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import us.solute.trackservice.api.TrackUpdate

/**
  * The Track Service stream interface.
  *
  * This describes everything that Lagom needs to know about how to serve and
  * consume the TrackServiceStream service.
  */
trait TrackServiceStreamService extends Service {

  def stream: ServiceCall[NotUsed, Source[TrackUpdate, NotUsed]]

  override final def descriptor: Descriptor = {
    import Service._

    named("track-service-stream")
      .withCalls(
        namedCall("stream", stream _)
      ).withAutoAcl(true)

  }
}

