package us.solute.trackservice.stream.impl

import java.util.UUID

import akka.{Done, NotUsed}
import akka.stream.scaladsl.{Flow, Source}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import us.solute.trackservice.api.{TrackService, TrackUpdate}
import us.solute.trackservice.stream.api.TrackServiceStreamService

import scala.concurrent.Future
import com.lightbend.lagom.scaladsl.pubsub.PubSubRegistry
import com.lightbend.lagom.scaladsl.pubsub.TopicId

/**
  * Implementation of the TrackServiceStreamService.
  */
class TrackServiceStreamServiceImpl(trackService: TrackService, pubSub: PubSubRegistry) extends TrackServiceStreamService {

  val topic = pubSub.refFor(TopicId[TrackUpdate]("myTopic"))

  trackService.tracksTopic().subscribe.atLeastOnce(
    Flow.fromFunction(trackUpdate => {
      topic.publish(trackUpdate)
      Done
    }))

  def stream = ServiceCall { _ => {
    val topic = pubSub.refFor(TopicId[TrackUpdate]("myTopic"))
    Future.successful(topic.subscriber)
  }
  }

}

