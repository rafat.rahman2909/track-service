package us.solute.trackservice.stream.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.trackservice.api.{Position, TrackService, TrackUpdate}
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaClientComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.pubsub.PubSubComponents
import com.softwaremill.macwire._
import us.solute.trackservice.stream.api.TrackServiceStreamService

import scala.collection.immutable.Seq

class TrackServiceStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TrackServiceStreamApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TrackServiceStreamApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[TrackServiceStreamService])
}

abstract class TrackServiceStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with LagomKafkaClientComponents
    with ClusterComponents
    with PubSubComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[TrackServiceStreamService](wire[TrackServiceStreamServiceImpl])

  override lazy val jsonSerializerRegistry = TrackServiceStramSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]
}


object TrackServiceStramSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[TrackUpdate],
    JsonSerializer[Position]
  )
}