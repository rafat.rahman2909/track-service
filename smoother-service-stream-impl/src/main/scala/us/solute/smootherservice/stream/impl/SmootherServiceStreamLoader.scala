package us.solute.smootherservice.stream.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.trackservice.api.{Position, TrackService, TrackUpdate}
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaClientComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.pubsub.PubSubComponents
import com.softwaremill.macwire._
import us.solute.smootherservice.api.{SmoothedAOU, SmootherService}
import us.solute.smootherservice.stream.api.SmootherServiceStream

import scala.collection.immutable.Seq

class SmootherServiceStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new SmootherServiceStreamApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new SmootherServiceStreamApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[SmootherServiceStream])
}

abstract class SmootherServiceStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with LagomKafkaClientComponents
    with ClusterComponents
    with PubSubComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[SmootherServiceStream](wire[SmootherServiceStreamImpl])

  override lazy val jsonSerializerRegistry = SmootherServiceStreamSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]

  // Bind the SmootherService client
  lazy val smootherService: SmootherService = serviceClient.implement[SmootherService]
}


object SmootherServiceStreamSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[SmoothedAOU]
  )
}