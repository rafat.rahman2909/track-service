package us.solute.smootherservice.stream.impl

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.api.ServiceCall
import us.solute.smootherservice.stream.api.SmootherServiceStream

import scala.concurrent.Future
import com.lightbend.lagom.scaladsl.pubsub.PubSubRegistry
import com.lightbend.lagom.scaladsl.pubsub.TopicId
import us.solute.smootherservice.api.{SmoothedAOU, SmootherService}

/**
  * Implementation of the SmootherServiceStreamService.
  */
class SmootherServiceStreamImpl(smootherService: SmootherService, pubSub: PubSubRegistry) extends SmootherServiceStream {

  val topic = pubSub.refFor(TopicId[SmoothedAOU]("smootherTopic"))

  smootherService.smoothedTopic().subscribe.atLeastOnce(
    Flow.fromFunction(smoothedAou => {
      topic.publish(smoothedAou)
      Done
    }))

  def smootherStream = ServiceCall { _ => {
    val topic = pubSub.refFor(TopicId[SmoothedAOU]("smootherTopic"))
    Future.successful(topic.subscriber)
  }
  }

}

