package us.solute.sensorservice.stream.api

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import us.solute.sensor.service.api.AOUUpdate

/**
  * The Sensor Service stream interface.
  *
  * This describes everything that Lagom needs to know about how to serve and
  * consume the SensorServiceStream service.
  */
trait SensorServiceStream extends Service {

  def sensorStream: ServiceCall[NotUsed, Source[AOUUpdate, NotUsed]]

  override final def descriptor: Descriptor = {
    import Service._

    named("sensor-service-stream")
      .withCalls(
        namedCall("sensorstream", sensorStream _)
      ).withAutoAcl(true)
  }
}

