package us.solute.sensor.service.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}
import us.solute.trackservice.api.Position


trait SensorService extends Service {

  object SensorService  {
    val TOPIC_NAME = "sensor"
  }

  /**
    * Example: curl http://localhost:9000/api/aou/0
    */
  def aou(id: String): ServiceCall[NotUsed, AOUUpdate]

  /**
    * Example: curl http://localhost:9000/api/aou
    */
  def updateAOU: ServiceCall[AOUUpdate, Done]

  /**
  * Example: curl http://localhost:9000/api/aou
  */
  def aous(): ServiceCall[NotUsed, List[AOUUpdate]]


  /**
    * Example: curl http://localhost:9000/api/aou/start
    */
  def startAOU: ServiceCall[UserInput, Done]

  /**
  * This gets published to Kafka.
  */
   def aouTopic(): Topic[AOUUpdate]

  override final def descriptor: Descriptor = {
    import Service._
    // @formatter:off
    named("sensor-service")
      .withCalls(
        pathCall("/api/aou/:id", aou _),
        pathCall("/api/aou", updateAOU _),
        pathCall("/api/aou", aous _),
        pathCall("/api/aou/start", startAOU _),
      )
      .withTopics(
        topic(SensorService.TOPIC_NAME, aouTopic _))
      .withAutoAcl(true)
    // @formatter:on
  }
}

case class AOUUpdate(id: String, centerPt: Position, ellipse: Ellipse, trackLat: Double, trackLong: Double, time: String)
object AOUUpdate {
  implicit val format: Format[AOUUpdate] = Json.format[AOUUpdate]
}

// smaj - long radii of ellipse
// smin - short radii of ellipse
// bearing - direction of ellipse
case class Ellipse(smaj: Double, smin:Double, bearing: Double)
object Ellipse {
  implicit  val format: Format[Ellipse] = Json.format[Ellipse]
}

case class Bias(mean: Double, percentDistribution: Double, distWidth:Double)
object Bias {
  implicit val format: Format[Bias] = Json.format[Bias]
}

case class UserInput(sminBias: Bias, smajBias: Bias, bearingBias: Bias, centerPtBias: Bias, timeOnLeg: Double,
                     timeWarp: Double, sensorEventTime: Double)
object UserInput {
  implicit val format: Format[UserInput] = Json.format[UserInput]
}