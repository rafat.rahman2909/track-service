package us.solute.kalmanservice.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.kalmanservice.api.KalmanService
import us.solute.sensor.service.api.SensorService
import us.solute.trackservice.api.TrackService

class KalmanServiceLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new KalmanServiceApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new KalmanServiceApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[KalmanService])
}

abstract class KalmanServiceApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with LagomKafkaComponents
    with ClusterComponents
    with CassandraPersistenceComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[KalmanService](wire[KalmanServiceImpl])

  override lazy val jsonSerializerRegistry = KalmanServiceSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]

  // Bind the SensorService client
  lazy val sensorService: SensorService = serviceClient.implement[SensorService]

  // Register the Sensor Service Simulator persistent entity
  persistentEntityRegistry.register(wire[KalmanServiceEntity])
}