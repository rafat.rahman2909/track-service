package us.solute.kalmanservice.impl

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.ReadSideProcessor.ReadSideHandler
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, ReadSideProcessor}
import us.solute.kalmanservice.api.KalmanAOU

import scala.concurrent.{ExecutionContext, Future}

class KalmanServiceEventProcessor(session: CassandraSession, readSide: CassandraReadSide)(implicit ec: ExecutionContext)
  extends ReadSideProcessor[KalmanServiceEvent] {

  val createTableStatement = """CREATE TABLE IF NOT EXISTS kalmanaouupdate (
  id TEXT,
  x_k TEXT,
  p_k TEXT,
  PRIMARY KEY (id)
)
"""
  // Writes
  val updateKalmanAOUStatement = """INSERT INTO kalmanaouupdate (id, x_k, p_k) VALUES (?, ?, ?)"""
  var updateKalmanAOU: PreparedStatement = _
  // Reads
  val getKalmanAOUUpdatesStatement = """SELECT * FROM kalmanaouupdate"""
  var getKalmanAOUUpdatesPrepared: PreparedStatement = _

  def getKalmanAOUUpdates(): Future[List[KalmanAOU]] = {
    session.selectAll(getKalmanAOUUpdatesPrepared.bind())
      .map(rows => rows.map(row => KalmanAOU(row.getString("id"), row.getString("x_k"), row.getString("p_k"))).toList)
  }

  def bindKalmanAOUUpdated(kalmanAou: KalmanAOU): BoundStatement = {
    val boundStatement = updateKalmanAOU.bind()
    boundStatement.setString("id", kalmanAou.id)
    boundStatement.setString("x_k", kalmanAou.x_k)
    boundStatement.setString("p_k", kalmanAou.p_k)
    boundStatement
  }

  def processAOUUpdate(event: KalmanAOUUpdated): Future[List[BoundStatement]] = {
    Future.successful(List(bindKalmanAOUUpdated(event.kalmanAou)))
  }

  override def buildHandler(): ReadSideHandler[KalmanServiceEvent] = readSide.builder[KalmanServiceEvent]("kalmanaouupdateoffset")
    .setGlobalPrepare(() =>
      for {
        _ <- session.executeCreateTable(createTableStatement)
      } yield Done)
    .setPrepare(_ =>
      for {
        _updateKalmanAOU <- session.prepare(updateKalmanAOUStatement)
        _getKalmanAOUUpdates <- session.prepare(getKalmanAOUUpdatesStatement)
      } yield {
        updateKalmanAOU = _updateKalmanAOU
        getKalmanAOUUpdatesPrepared = _getKalmanAOUUpdates
        Done
      })
    .setEventHandler[KalmanAOUUpdated](e => processAOUUpdate(e.event))
    .build()

  override def aggregateTags: Set[AggregateEventTag[KalmanServiceEvent]] = KalmanServiceEvent.Tag.allTags
}
