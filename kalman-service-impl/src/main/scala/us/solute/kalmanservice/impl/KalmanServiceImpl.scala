package us.solute.kalmanservice.impl

import java.util

import breeze.linalg._
import breeze.numerics._
import akka.{Done, NotUsed}
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import us.solute.kalmanservice.api.{KalmanAOU, KalmanService}
import us.solute.sensor.service.api.{AOUUpdate, Ellipse, SensorService, UserInput}
import us.solute.trackservice.api.Position

import scala.concurrent.{ExecutionContext, Future}

/**
  * Implementation of the KalmanServiceStreamService.
  */
class KalmanServiceImpl(sensorService: SensorService, persistentEntityRegistry: PersistentEntityRegistry, readSide: ReadSide,
                        cassandraReadSide: CassandraReadSide,
                        cassandraSession: CassandraSession)(implicit ec: ExecutionContext) extends KalmanService {

  val eventProcessor = new KalmanServiceEventProcessor(cassandraSession, cassandraReadSide)

  readSide.register[KalmanServiceEvent](eventProcessor)

  var aouList : List[AOUUpdate] = List()
  val earth_R = 6378.1 * 1000
  var servicesStarted = false
  var iterations = 1

  // time s
  private var s = 0d
  // Prediction matrix also known as state transition matrix
  private var F_k = DenseMatrix((1d, 0d, s, 0d), (0d, 1d, 0d, s), (0d, 0d, 1d, 0d), (0d, 0d, 0d, 1d))
  // Control matrix
  private val B_k = DenseMatrix.eye[Double](4)
  // Control vector
  private val u_k =DenseMatrix.eye[Double](4)
  private var stdDev = 0
  private var B = 1
  // Covariance [0 0 0 0 ][0 0 0 0 ][0 0 stdDev2/2B 0 ][0 0 0 stdDev2/2B]
  private val Q_k = DenseMatrix((0d, 0d, 0d, 0d), (0d, 0d, 0d, 0d), (0d, 0d, (stdDev/2*B).toDouble, 0d), (0d, 0d, 0d, 1d))
  // Measurement Matrix
  private val H_k = DenseMatrix.eye[Double](4)
  // Covariance of uncertainty
  private val R_k = DenseMatrix.eye[Double](4)
  // mean
  private val z_k = DenseMatrix.eye[Double](4)
  private var X_hat = DenseMatrix.eye[Double](4)
  private var P_hat = DenseMatrix.eye[Double](4)
  private var X_k = DenseMatrix.eye[Double](4)
  private var P_k = DenseMatrix.eye[Double](4)

  sensorService.aouTopic().subscribe.atLeastOnce(
    Flow.fromFunction(aouUpdate => {
      if (servicesStarted) {
        val ref = persistentEntityRegistry.refFor[KalmanServiceEntity](aouUpdate.id)
        val kalman = kalmanFilter(aouUpdate, aouUpdate.id)
        ref.ask(UpdateKalmanAOU(kalman: KalmanAOU))
      }
      Done
    }))

  /*
  @input aous: List of incoming sensor hits
  @input id: Track ID
  @output KalmanAOU: Position of predicted track
   */
  private def kalmanFilter(aou: AOUUpdate, id: String): KalmanAOU = {
    s = s + aou.time.toDouble
    for(_ <- 1 to iterations) {
      val (x, p) = predict(X_hat, P_k, F_k, B_k, u_k, Q_k)
      X_hat = x
      P_hat = p
      val (x2, p2) = update(X_hat, P_hat, z_k, R_k, H_k)
      X_k = x2
      P_k = p2

      X_hat = X_k
      P_hat = P_k
    }

    val newAOU = KalmanAOU(id, X_hat.toArray.mkString, P_hat.toArray.mkString)
    println(newAOU)
    // s is changed, update F_k
    F_k = DenseMatrix((1d, 0d, s, 0d), (0d, 1d, 0d, s), (0d, 0d, 1d, 0d), (0d, 0d, 0d, 1d))

    aouList = aouList.dropRight(1)
    newAOU
  }

  /*
  @input X_hat_k1: Previous kalman filter calculation
  @input P_k1: Previous kalman filter noise calculation
  @input F_k: Prediction Matrix
  @input B_k: Control matrix
  @input u_k: Control Vector
  @input Q_k: Covariance
  @output Calculated x and p prediction matrices
   */
  private def predict(X_hat_k1: DenseMatrix[Double], P_k1: DenseMatrix[Double], F_k: DenseMatrix[Double],
                      B_k: DenseMatrix[Double], u_k: DenseMatrix[Double], Q_k: DenseMatrix[Double]):
  (DenseMatrix[Double], DenseMatrix[Double]) = {
    val X = F_k * X_hat_k1 + B_k * u_k
    val P = F_k*P_k1*F_k.t + Q_k
    (X, P)
  }

  /*
  @input X_hat_k1: Predicted kalman filter calculation
  @input P_k1: Predicted kalman filter noise calculation
  @input z_k: Mean
  @input R_k: Covariance of uncertainty
  @input H_k: Matrix modeling sensors
  @output Calculated x and p updated matrices
   */
  private def update (X_hat_k: DenseMatrix[Double], P_k: DenseMatrix[Double], z_k: DenseMatrix[Double],
                      R_k: DenseMatrix[Double], H_k: DenseMatrix[Double]): (DenseMatrix[Double], DenseMatrix[Double])  = {
    val KPrime = P_k*H_k.t*inv(H_k*P_k*H_k.t + R_k)
    val X = X_hat_k + KPrime*(z_k-H_k*X_hat_k)
    val P = P_k- KPrime*H_k*P_k
    (X, P)
  }

  /**
    * This gets published to Kafka.
    */
  override def kalmanTopic(): Topic[KalmanAOU] = {
    TopicProducer.taggedStreamWithOffset(KalmanServiceEvent.Tag) { (eventStream, offset) =>
      persistentEntityRegistry.eventStream(eventStream, offset).map(ev => (convertEvent(ev), ev.offset))
    }
  }

  private def convertEvent(trackEvent: EventStreamElement[KalmanServiceEvent]): KalmanAOU = {
    trackEvent.event match {
      case KalmanAOUUpdated(kalmanAouUpdate) => kalmanAouUpdate
    }
  }

  /**
    * Example: curl http://localhost:9000/api/kalmanaou/0
    */
  override def kalmanAou(id: String) = ServiceCall { _ =>
    // Look up the Kalman Service entity for the given ID.
    val ref = persistentEntityRegistry.refFor[KalmanServiceEntity](id)
    // Ask the entity the Hello command.
    ref.ask(GetKalmanAOU)
  }

  /**
    * Example: curl http://localhost:9000/api/kalmanaou
    */
  override def updateKalmanAOU()= ServiceCall { kalmanAou =>
    // Look up the Track Service Sensor entity for the given ID.
    val ref = persistentEntityRegistry.refFor[KalmanServiceEntity](kalmanAou.id)
    // Tell the entity to use the greeting message specified.
    ref.ask(UpdateKalmanAOU(kalmanAou: KalmanAOU))
  }

  /**
    * Example: curl http://localhost:9000/api/kalmanaou/start
    */
  override def startKalmanAOU() = ServiceCall { input =>
    servicesStarted = true
    iterations = input.iterations.toInt
    Future.successful(Done)
  }

  /**
   * Example: curl http://localhost:9000/api/aou
   */
  override def kalmanAous= ServiceCall { _ =>
    eventProcessor.getKalmanAOUUpdates()
  }
}
