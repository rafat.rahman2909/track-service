package us.solute.smootherservice.impl

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import us.solute.sensor.service.api.{AOUUpdate, SensorService}
import us.solute.smootherservice.api.{SmoothedAOU, SmootherService}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Implementation of the SmootherServiceStreamService.
  */
class SmootherServiceImpl(
  sensorService: SensorService,
  persistentEntityRegistry: PersistentEntityRegistry,
  readSide: ReadSide,
  cassandraReadSide: CassandraReadSide,
  cassandraSession: CassandraSession
)(implicit ec: ExecutionContext)
    extends SmootherService {

  val eventProcessor =
    new SmootherServiceEventProcessor(cassandraSession, cassandraReadSide)

  readSide.register[SmootherServiceEvent](eventProcessor)

  var aouList: List[AOUUpdate] = List()
  var servicesStarted = false
  var tracksPerIter = 5.toDouble

  sensorService
    .aouTopic()
    .subscribe
    .atLeastOnce(Flow.fromFunction(aouUpdate => {
      aouList ::= aouUpdate
      if (aouList.length > tracksPerIter && servicesStarted) {
        val ref =
          persistentEntityRegistry.refFor[SmootherServiceEntity](aouUpdate.id)
        val (smoothed, newAouList) = MovingAverage.movingAverage(aouList, aouUpdate.id)
        aouList = newAouList
        ref.ask(UpdateSmoothedAOU(smoothed: SmoothedAOU))
      }
      Done
    }))

  /**
    * This gets published to Kafka.
    */
  override def smoothedTopic(): Topic[SmoothedAOU] = {
    TopicProducer.taggedStreamWithOffset(SmootherServiceEvent.Tag) {
      (eventStream, offset) =>
        persistentEntityRegistry
          .eventStream(eventStream, offset)
          .map(ev => (convertEvent(ev), ev.offset))
    }
  }

  private def convertEvent(
    trackEvent: EventStreamElement[SmootherServiceEvent]
  ): SmoothedAOU = {
    trackEvent.event match {
      case SmoothedAOUUpdated(smoothedAouUpdate) => smoothedAouUpdate
    }
  }

  /**
    * Example: curl http://localhost:9000/api/smoothedaou/0
    */
  override def smoothedAou(id: String) = ServiceCall { _ =>
    // Look up the Smoothed Service entity for the given ID.
    val ref = persistentEntityRegistry.refFor[SmootherServiceEntity](id)
    // Ask the entity the Hello command.
    ref.ask(GetSmoothedAOU)
  }

  /**
    * Example: curl http://localhost:9000/api/smoothedaou
    */
  override def updateSmoothedAOU() = ServiceCall { smoothedAou =>
    // Look up the Track Service Sensor entity for the given ID.
    val ref =
      persistentEntityRegistry.refFor[SmootherServiceEntity](smoothedAou.id)
    // Tell the entity to use the greeting message specified.
    ref.ask(UpdateSmoothedAOU(smoothedAou: SmoothedAOU))
  }

  /**
    * Example: curl http://localhost:9000/api/smoothedaou/start
    */
  override def startSmoothedAOU() = ServiceCall { input =>
    servicesStarted = true
    tracksPerIter = input.tracksPerIteration
    Future.successful(Done)
  }

  /**
    * Example: curl http://localhost:9000/api/aou
    */
  override def smoothedAous = ServiceCall { _ =>
    eventProcessor.getSmoothedAOUUpdates()
  }
}
