package us.solute.smootherservice.impl

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.ReadSideProcessor.ReadSideHandler
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, ReadSideProcessor}
import us.solute.sensor.service.api.Ellipse
import us.solute.smootherservice.api.SmoothedAOU
import us.solute.trackservice.api.Position

import scala.concurrent.{ExecutionContext, Future}

class SmootherServiceEventProcessor(session: CassandraSession, readSide: CassandraReadSide)(implicit ec: ExecutionContext)
  extends ReadSideProcessor[SmootherServiceEvent] {

  val createTableStatement = """CREATE TABLE IF NOT EXISTS smoothedaouupdate (
  id TEXT,
  alt TEXT,
  lat DOUBLE,
  lon DOUBLE,
  smaj DOUBLE,
  smin DOUBLE,
  bearing DOUBLE,
  trackLat DOUBLE,
  trackLong DOUBLE,
  time TEXT,
  PRIMARY KEY (id)
)
"""
  // Writes
  val updateSmoothedAOUStatement = """INSERT INTO smoothedaouupdate (id, alt, lat, lon, smaj, smin, bearing, trackLat, trackLong, time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
  var updateSmoothedAOU: PreparedStatement = _
  // Reads
  val getSmoothedAOUUpdatesStatement = """SELECT * FROM smoothedaouupdate"""
  var getSmoothedAOUUpdatesPrepared: PreparedStatement = _

  def getSmoothedAOUUpdates(): Future[List[SmoothedAOU]] = {
    session.selectAll(getSmoothedAOUUpdatesPrepared.bind())
      .map(rows => rows.map(row => SmoothedAOU(row.getString("id"), Position(row.getString("alt"),
        row.getDouble("lat"), row.getDouble("lon")), Ellipse(row.getDouble("smaj"), row.getDouble("smin"),
          row.getDouble("bearing")), row.getDouble("trackLat"), row.getDouble("trackLong"), row.getString("time") )).toList)
  }

  def bindSmoothedAOUUpdated(smoothedAou: SmoothedAOU): BoundStatement = {
    val boundStatement = updateSmoothedAOU.bind()
    boundStatement.setString("id", smoothedAou.id)
    boundStatement.setString("alt", smoothedAou.centerPt.alt)
    boundStatement.setDouble("lat", smoothedAou.centerPt.latitude)
    boundStatement.setDouble("lon", smoothedAou.centerPt.longitude)
    boundStatement.setDouble("smaj", smoothedAou.ellipse.smaj)
    boundStatement.setDouble("smin", smoothedAou.ellipse.smin)
    boundStatement.setDouble("bearing", smoothedAou.ellipse.bearing)
    boundStatement.setDouble("trackLat", smoothedAou.trackLat)
    boundStatement.setDouble("trackLong", smoothedAou.trackLong)
    boundStatement.setString("time", smoothedAou.time)
    boundStatement
  }

  def processAOUUpdate(event: SmoothedAOUUpdated): Future[List[BoundStatement]] = {
    Future.successful(List(bindSmoothedAOUUpdated(event.smoothedAou)))
  }

  override def buildHandler(): ReadSideHandler[SmootherServiceEvent] = readSide.builder[SmootherServiceEvent]("smoothedaouupdateoffset")
    .setGlobalPrepare(() =>
      for {
        _ <- session.executeCreateTable(createTableStatement)
      } yield Done)
    .setPrepare(_ =>
      for {
        _updateSmoothedAOU <- session.prepare(updateSmoothedAOUStatement)
        _getSmoothedAOUUpdates <- session.prepare(getSmoothedAOUUpdatesStatement)
      } yield {
        updateSmoothedAOU = _updateSmoothedAOU
        getSmoothedAOUUpdatesPrepared = _getSmoothedAOUUpdates
        Done
      })
    .setEventHandler[SmoothedAOUUpdated](e => processAOUUpdate(e.event))
    .build()

  override def aggregateTags: Set[AggregateEventTag[SmootherServiceEvent]] = SmootherServiceEvent.Tag.allTags
}
