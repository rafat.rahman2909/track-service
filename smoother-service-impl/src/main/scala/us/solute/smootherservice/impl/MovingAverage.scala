package us.solute.smootherservice.impl

import us.solute.geoutils.{Bearing, GeoUtil}
import us.solute.sensor.service.api.{AOUUpdate, Ellipse}
import us.solute.smootherservice.api.SmoothedAOU
import us.solute.trackservice.api.{Cart, Position}

import scala.util.Try

object MovingAverage {
  private val tracksPerIter = 5.toDouble
  Bearing

  /*
  @input aous: List of AOUUpdate objects being streamed in
  @input id: Track/AOU ID
  @output SmoothedAOU object which is a moving average ellipse based on previous sensor AOUs
   */
  def movingAverage(aous: List[AOUUpdate], id: String): (SmoothedAOU, List[AOUUpdate]) = {

    val tempAouList = aous.takeRight(tracksPerIter.toInt)
    var pts: List[Cart] = List()

    for (i <- tempAouList) {
      pts ::= GeoUtil.geoToCart(i.centerPt.latitude, i.centerPt.longitude)
    }

    var mvavg: List[Position] = List()
    var avgell: List[Ellipse] = List()
    var avgdt: List[Long] = List()
    for (i <- 1 until pts.length - 1) {
      val pt = Cart(
        (pts(i - 1).x + pts(i).x + pts(i + 1).x) / 3,
        (pts(i - 1).y + pts(i).y + pts(i + 1).y) / 3,
        (pts(i - 1).z + pts(i).z + pts(i + 1).z) / 3
      )
      mvavg ::= GeoUtil.cartToGeo(pt)
      val smaj = (tempAouList(i - 1).ellipse.smaj + tempAouList(i).ellipse.smaj + tempAouList(
        i + 1
      ).ellipse.smaj) / 3
      val smin = (tempAouList(i - 1).ellipse.smin + tempAouList(i).ellipse.smin + tempAouList(
        i + 1
      ).ellipse.smin) / 3
      val bearing = (tempAouList(i - 1).ellipse.bearing + tempAouList(i).ellipse.bearing +
        tempAouList(i + 1).ellipse.bearing) / 3
      avgell ::= Ellipse(smaj, smin, bearing)
      avgdt ::= (tempAouList(i - 1).time.toLong + tempAouList(i).time.toLong + tempAouList(
        i + 1
      ).time.toLong) / 3
    }

    val d1 = GeoUtil.geoDistance(
      mvavg.head.latitude,
      mvavg.head.longitude,
      mvavg(1).latitude,
      mvavg(1).longitude
    )
    val d2 = GeoUtil.geoDistance(
      mvavg(1).latitude,
      mvavg(1).longitude,
      mvavg(2).latitude,
      mvavg(2).longitude
    )

    val finalBear = GeoUtil.geoFinalBearing(
      mvavg(1).latitude,
      mvavg(1).longitude,
      mvavg(2).latitude,
      mvavg(2).longitude
    )
    // check for divison by 0
    var avgdt_1 = 0.0d
    if (avgdt(1) - avgdt.head != 0) {
      avgdt_1 = d1 / (avgdt(1) - avgdt.head)
    }

    var avgdt_2 = 0.0d
    if (avgdt(2) - avgdt(1) != 0) {
      avgdt_2 = d2 / (avgdt(2) - avgdt(1))
    }

    val avgspeed = (avgdt_1 + avgdt_2) / 2

    val timeElapsed = System.nanoTime() - avgdt(2)

    val dest = GeoUtil.geoMove(
      mvavg(2).latitude,
      mvavg(2).longitude,
      avgspeed,
      finalBear,
      timeElapsed
    )
    val newAOU = SmoothedAOU(
      id,
      dest,
      avgell.head,
      mvavg.head.latitude,
      mvavg.head.longitude,
      avgdt(2).toString()
    )

    val newAOUList = aous.dropRight(1)
    (newAOU, newAOUList)
  }
}
