package us.solute.smootherservice.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.smootherservice.api.SmootherService
import us.solute.sensor.service.api.SensorService
import us.solute.trackservice.api.TrackService

class SmootherServiceLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new SmootherServiceApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new SmootherServiceApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[SmootherService])
}

abstract class SmootherServiceApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with LagomKafkaComponents
    with ClusterComponents
    with CassandraPersistenceComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[SmootherService](wire[SmootherServiceImpl])

  override lazy val jsonSerializerRegistry = SmootherServiceSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]

  // Bind the SensorService client
  lazy val sensorService: SensorService = serviceClient.implement[SensorService]

  // Register the Sensor Service Simulator persistent entity
  persistentEntityRegistry.register(wire[SmootherServiceEntity])
}