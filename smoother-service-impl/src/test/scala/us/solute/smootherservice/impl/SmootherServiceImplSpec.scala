package us.solute.smootherservice.impl

import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import org.scalamock.scalatest.MockFactory
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}
import us.solute.sensor.service.api.{AOUUpdate, Ellipse}
import us.solute.smootherservice.api.SmootherService
import us.solute.trackservice.api.Position

class SmootherServiceImplSpec
    extends AsyncWordSpec
    with Matchers
    with BeforeAndAfterAll
    with MockFactory {

  private val server = ServiceTest.startServer(
    ServiceTest.defaultSetup
      .withCassandra()
  ) { ctx =>
    new SmootherServiceApplication(ctx) with LocalServiceLocator
  }

  val client: SmootherService = server.serviceClient.implement[SmootherService]

  override protected def afterAll(): Unit = server.stop()

  var position1 = new Position("0", 0, 0)
  var ellipse1 = new Ellipse(10, 10, 0)
  var aouUpdate1 = new AOUUpdate("id", position1, ellipse1, position1.latitude, position1.longitude, "10")
  var aouUpdateList: List[AOUUpdate] = List(aouUpdate1, aouUpdate1, aouUpdate1, aouUpdate1, aouUpdate1)


  "SmootherServiceImpl" should {
    "do stuff " in {
      // TODO: add tests
      assert(true == true)
    }
  }
}
