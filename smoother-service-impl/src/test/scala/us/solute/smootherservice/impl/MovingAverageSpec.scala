package us.solute.smootherservice.impl

import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import us.solute.sensor.service.api.{AOUUpdate, Ellipse}
import us.solute.smootherservice.api.SmoothedAOU
import us.solute.trackservice.api.Position

class MovingAverageSpec extends WordSpec with Matchers with BeforeAndAfterAll {

  "MovingAverage" can {
    "movingAverage" should {
      "return a smoothedAOU in the same location if all aouUpdates are same location" in {
        val position1 = new Position("0", 0, 0)
        val ellipse1 = new Ellipse(10, 10, 0)
        val aouUpdate1 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "10"
        )
        val aouUpdate2 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "20"
        )
        val aouUpdate3 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "30"
        )
        val aouUpdate4 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "40"
        )
        val aouUpdate5 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "50"
        )
        //  var aouUpdateList: List[AOUUpdate] = List(aouUpdate1, aouUpdate1, aouUpdate1, aouUpdate1, aouUpdate1)
        val aouUpdateList: List[AOUUpdate] =
          List(aouUpdate1, aouUpdate2, aouUpdate3, aouUpdate4, aouUpdate5)
        val output: SmoothedAOU = new SmoothedAOU(
          "id",
          Position("0", 0.0, 0.0),
          Ellipse(10.0, 10.0, 0.0),
          0.0,
          0.0,
          "20"
        )

        val (smoothedAOU, discardedList) =
          MovingAverage.movingAverage(aouUpdateList, "id")
        assert(smoothedAOU === output)
      }
      "take the average when moving north" in {
        val systemTime = System.nanoTime()

        val position1 = new Position("0", 0, 0)
        val ellipse1 = new Ellipse(5, 5, 0)
        val aouUpdate1 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          (systemTime - 30).toString
        )
//        val position2 = new Position("0", 5, 0)
//        val ellipse1 = new Ellipse(5, 5, 0)
        val aouUpdate2 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "20"
        )
        val aouUpdate3 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "30"
        )
        val aouUpdate4 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "40"
        )
        val aouUpdate5 = new AOUUpdate(
          "id",
          position1,
          ellipse1,
          position1.latitude,
          position1.longitude,
          "50"
        )
        //  var aouUpdateList: List[AOUUpdate] = List(aouUpdate1, aouUpdate1, aouUpdate1, aouUpdate1, aouUpdate1)
        val aouUpdateList: List[AOUUpdate] =
          List(aouUpdate1, aouUpdate2, aouUpdate3, aouUpdate4, aouUpdate5)
        val output: SmoothedAOU = new SmoothedAOU(
          "id",
          Position("0", 0.0, 0.0),
          Ellipse(10.0, 10.0, 0.0),
          0.0,
          0.0,
          "20"
        )
      }
    }
  }
}
