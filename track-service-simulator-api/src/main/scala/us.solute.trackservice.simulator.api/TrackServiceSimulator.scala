package us.solute.trackservice.simulator.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}

import scala.concurrent.duration.FiniteDuration

trait TrackServiceSimulator extends Service {

  def startSimulation(): ServiceCall[SimulatorProps, Done]

  def stopSimulation(): ServiceCall[NotUsed, Done]

  override final def descriptor: Descriptor = {
    import Service._
    // @formatter:off
    named("track-service-simulator")
      .withCalls(
        pathCall("/api/tracks/simulator/start", startSimulation _),
        pathCall("/api/tracks/simulator/stop", stopSimulation _),
      )
      .withAutoAcl(true)
    // @formatter:on
  }
}

case class SimulatorProps(boundingBox: BoundingBox, physics: Physics, trackCount: Int, period: String, duration: String)
object SimulatorProps {
  implicit val format: Format[SimulatorProps] = Json.format[SimulatorProps]
}

case class BoundingBox(upperLeft: Position, lowerRight: Position)
object BoundingBox {
  implicit val format: Format[BoundingBox] = Json.format[BoundingBox]
}

case class Physics(speed: Double, bearing: Double, timeWarp: Double, timeOnLeg: Double)
object Physics {
  implicit  val format: Format[Physics] = Json.format[Physics]
}

case class Position(latitude: Double, longitude: Double)
object Position {
  implicit val format: Format[Position] = Json.format[Position]
}
