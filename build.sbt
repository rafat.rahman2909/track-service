import com.lightbend.lagom.core.LagomVersion

organization in ThisBuild := "us.solute"

version in ThisBuild ~= (_.replace('+', '-'))
dynver in ThisBuild ~= (_.replace('+', '-'))

lagomCassandraCleanOnStart in ThisBuild := true
//lagomServiceLocatorAddress in ThisBuild := "10.3.3.78"
//lagomServiceGatewayAddress in ThisBuild := "10.3.3.78"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.12.8"

dockerBaseImage := "adoptopenjdk/openjdk8"
dockerUpdateLatest := true
dockerUsername := sys.props.get("docker.username")
dockerRepository := sys.props.get("docker.registry")

val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided"
val scalaMock = "org.scalamock" %% "scalamock" % "4.4.0" % Test
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.4" % Test
val akkaDiscoveryKubernetesApi = "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % "1.0.0"
val akkaDiscovery = "com.lightbend.lagom" %% "lagom-scaladsl-akka-discovery-service-locator" % LagomVersion.current

val nd4jVersion = "0.9.1"

//lazy val `cors-scala` = (project in file("."))
//      .aggregate(`track-service-api`, `track-service-impl`)

lazy val `track-service` = (project in file("."))
  .aggregate(`geo-utils`, `track-service-api`, `track-service-impl`, `track-service-stream-api`, `track-service-stream-impl`, `track-service-simulator-api`, `track-service-simulator-impl`)

lazy val `geo-utils` = (project in file("geo-utils"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslTestKit,
      scalaMock,
      scalaTest
  ))
  .dependsOn(`track-service-api`)

lazy val `track-service-api` = (project in file("track-service-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslTestKit,
      scalaMock,
      scalaTest
    )
  )

lazy val `track-service-impl` = (project in file("track-service-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest,
      filters
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`track-service-api`)

lazy val `track-service-stream-api` = (project in file("track-service-stream-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`)

lazy val `track-service-stream-impl` = (project in file("track-service-stream-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPubSub,
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`track-service-stream-api`, `track-service-api`)

lazy val `track-service-simulator-api` = (project in file("track-service-simulator-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`)

lazy val `track-service-simulator-impl` = (project in file("track-service-simulator-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPubSub,
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslTestKit,
      lagomScaladslKafkaBroker,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`track-service-simulator-api`, `track-service-api`)

lazy val `sensor-service-api` = (project in file("sensor-service-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`)

lazy val `sensor-service-impl` = (project in file("sensor-service-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      "org.apache.commons" % "commons-math3" % "3.0",
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslTestKit,
      lagomScaladslKafkaBroker,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`sensor-service-api`, `track-service-api`)

lazy val `smoother-service-api` = (project in file("smoother-service-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`,`sensor-service-api`)

lazy val `smoother-service-impl` = (project in file("smoother-service-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaMock,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`smoother-service-api`, `track-service-api`, `sensor-service-api`, `geo-utils`)

lazy val `sensor-service-stream-api` = (project in file("sensor-service-stream-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`, `sensor-service-api`)

lazy val `sensor-service-stream-impl` = (project in file("sensor-service-stream-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPubSub,
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`sensor-service-stream-api`, `track-service-api`, `sensor-service-api`)

lazy val `smoother-service-stream-api` = (project in file("smoother-service-stream-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`, `smoother-service-api`)

lazy val `smoother-service-stream-impl` = (project in file("smoother-service-stream-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPubSub,
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`smoother-service-stream-api`, `track-service-api`, `smoother-service-api`)

lazy val `kalman-service-api` = (project in file("kalman-service-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )
  .dependsOn(`track-service-api`,`sensor-service-api`)

lazy val `kalman-service-impl` = (project in file("kalman-service-impl"))
  .enablePlugins(UniversalPlugin, DockerPlugin, LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      akkaDiscovery,
      akkaDiscoveryKubernetesApi,
      macwire,
      scalaTest,
      "org.scalanlp" %% "breeze" % "1.0"
    )
  )
  .dependsOn(`kalman-service-api`, `track-service-api`, `sensor-service-api`)