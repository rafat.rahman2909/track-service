package us.solute.geoutils

/*
Utility class for bearing
@input degrees - compass degrees with 0 being north, 90 is east, 180 south, and 270 being west
 */
case class Bearing (degrees_0_to_360: Double) {
  /*
  @return bearing between [-180, 180) with 0 => 0
   */
  def relativeBearing: Double = {
    ((this.degrees_0_to_360 + 180 ) % 360 + 360) % 360 - 180
  }

  /*
  @return degrees in radians
   */
  def toRad: Double = {
    Math.toRadians(this.degrees_0_to_360)
  }

  /*
  @return relative bearing in radians
   */
  def toRadRelativeBearing: Double = {
    Math.toRadians(relativeBearing)
  }

  /*
  @return degrees between [0, 360)
   */
  def wrap360: Double = {
    (this.degrees_0_to_360 % 360 + 360) % 360
  }
}
