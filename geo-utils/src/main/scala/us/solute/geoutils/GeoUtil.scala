package us.solute.geoutils

import us.solute.trackservice.api.{Cart, Position}

object GeoUtil {
  val earth_R = 6378.137 * 1000

  /*
  @input lat: Double latitude for position that's being converted to cartesian coord
  @input long: Double longitude for position that's being converted to cartesian coord
  @output Cartesian coordinates of lat/long
   */
  def geoToCart(lat: Double, long: Double): Cart = {
    val lat1 = Math.toRadians(lat)
    val long1 = Math.toRadians(long)
    val x = earth_R * Math.cos(lat1) * Math.cos(long1)
    val y = earth_R * Math.cos(lat1) * Math.sin(long1)
    val z = earth_R * Math.sin(lat1)

    Cart(x.toDouble, y.toDouble, z.toDouble)
  }

  /*
  @input cart: Cartesian coordinates for position that's being converted to lat/long
  @output Lat/Long position based on cartesian coordinates
   */
  def cartToGeo(cart: Cart): Position = {
    val lat = Math.toDegrees(Math.asin(cart.z / earth_R))
    val long = Math.toDegrees(Math.atan2(cart.y, cart.x))
    Position("0", lat.toDouble, long.toDouble)
  }

  /*
  find the direction from point 2 to point 1 (end to start) and adds 180 to that angle to get the bearing at point 2 given starting location point 1
  @input lat1, lng1: Doubles for position of first point
  @input lat2, lng2: Doubles for position of second point so we can get the angle between both points
  @output bearing after accounting for earth ie =< 360 degrees
   */
  def geoFinalBearing(lat1: Double,
                      lng1: Double,
                      lat2: Double,
                      lng2: Double): Bearing = {
    val initialBearing = geoInitialBearing(lat2, lng2, lat1, lng1)
    Bearing(rangeFinalBearing(initialBearing))
  }

  /*
  finds the opposite direction of the initial bearing in range [0, 360)
  @input geoInitalBearing: Double for the initial bearing
  @output the final bearing in [0, 360]
   */
  def rangeFinalBearing(geoInitialBearing: Double): Double = {
    var finalBearing = (geoInitialBearing + 180) % 360
    if (finalBearing < 0) {
      finalBearing += 360
    }
    finalBearing
  }

  /*
  @input lat1, lng1: Doubles for position of first point
  @input lat2, lng2: Doubles for position of second point so we can get the angle between both points
  @output angle between two points from (lat1, lng1) to (lat2, lng2)
   */
  def geoInitialBearing(lat1: Double,
                        lng1: Double,
                        lat2: Double,
                        lng2: Double): Double = {
    val nlat1 = Math.toRadians(lat1)
    val nlng1 = Math.toRadians(lng1)
    val nlat2 = Math.toRadians(lat2)
    val nlng2 = Math.toRadians(lng2)
    val y = Math.sin(nlng2 - nlng1) * Math.cos(nlat2)
    val x = Math.cos(nlat1) * Math.sin(nlat2) - Math.sin(nlat1) * Math.cos(
      nlat2
    ) * Math.cos(nlng2 - nlng1)
    Math.toDegrees(Math.atan2(y, x))
  }

  /*
 @input lat1, lng1: Doubles for position of first point
 @input lat2, lng2: Doubles for position of second point so we can get the distance between both points
 @output Distance between two points while accounting for geo. Note this function is based on the haversine formula
   */
  def geoDistance(lat1: Double,
                  lng1: Double,
                  lat2: Double,
                  lng2: Double): Double = {
    val latDistance = Math.toRadians(lat1 - lat2)
    val lngDistance = Math.toRadians(lng1 - lng2)
    val sinLat = Math.sin(latDistance / 2)
    val sinLng = Math.sin(lngDistance / 2)
    val a = sinLat * sinLat +
      (Math.cos(Math.toRadians(lat1))
        * Math.cos(Math.toRadians(lat2))
        * sinLng * sinLng)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    (earth_R * c)
  }

  /*
  @input lat, lng: Doubles for position of starting point
  @input speed: Double for speed we want to move the point, using m/s
  @input brng: Double for angle we want to move the point at
  @input time: Double for time, used for calculating distance based on speed and time. Using nanoseconds
  @output New calculated position accounting for earth's geo
   */
  def geoMove(lat: Double,
              lng: Double,
              speed: Double,
              brng: Bearing,
              time: Double): Position = {
    val nlat = Math.toRadians(lat)
    val nlng = Math.toRadians(lng)
    val nBrng = Math.toRadians(brng.degrees_0_to_360)
    val distance = (speed * time) / 1e9
    val lat1 = Math.asin(
      Math.sin(nlat) * Math.cos(distance / earth_R) +
        Math.cos(nlat) * Math.sin(distance / earth_R) * Math.cos(nBrng)
    )
    val lng1 = nlng + Math.atan2(
      Math.sin(nBrng) * Math.sin(distance / earth_R) * Math.cos(nlat),
      Math.cos(distance / earth_R) - Math.sin(nlat) * Math.sin(lat1)
    )
    val normal_lat1 = Math.toDegrees(lat1)
    val normal_lng1 = ((Math.toDegrees(lng1) + 180) % 360 + 360) % 360 - 180

    Position("0", normal_lat1, normal_lng1)
  }
}
