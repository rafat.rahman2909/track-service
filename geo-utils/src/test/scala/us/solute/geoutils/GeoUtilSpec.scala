package us.solute.geoutils

import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import us.solute.trackservice.api.{Cart, Position}

import scala.collection.mutable.ListBuffer

class GeoUtilSpec extends WordSpec with Matchers with BeforeAndAfterAll {

  val epsilon = 1e-4d
  val distance_epsilon = 1e3d

  var position1 = new Position("0", 0, 0)
  var position2 = new Position("0", 0, 5)
  var position3 = new Position("0", 360, 365)
  var position4 = new Position("0", 90, 180)
  var position5 = new Position("0", 90, 12)
  var position6 = new Position("0", 90, -180)

  var cart1 = new Cart(6378137, 0, 0)
  var cart2 = new Cart(6353866.263102791, 555891.2675813203, 0.0)
  var cart4 =
    new Cart(-3.905482530786651E-10, 4.7828366804537766E-26, 6378137.0)

  "GeoUtil" can {
    "geoToCart" should {
      "convert lat / long to cartesian coords" in {
        assert(
          GeoUtil.geoToCart(position1.latitude, position1.longitude) === cart1
        )
        assert(
          GeoUtil.geoToCart(position2.latitude, position2.longitude) === cart2
        )
        assert(
          GeoUtil.geoToCart(position4.latitude, position4.longitude) === cart4
        )
      }
      "full 360 rotations map to the same location" in {
        val cart2 = GeoUtil.geoToCart(position2.latitude, position2.longitude)
        val cart3 = GeoUtil.geoToCart(position3.latitude, position3.longitude)
        assert(cart2.x === cart3.x +- epsilon)
        assert(cart2.y === cart3.y +- epsilon)
        assert(cart2.z === cart3.z +- epsilon)
      }
      "the north pole should map to the same location if the long is different" in {
        assert(
          GeoUtil
            .geoToCart(position4.latitude, position4.longitude)
            .x === GeoUtil
            .geoToCart(position5.latitude, position5.longitude)
            .x +- epsilon
        )
        assert(
          GeoUtil
            .geoToCart(position4.latitude, position4.longitude)
            .y === GeoUtil
            .geoToCart(position5.latitude, position5.longitude)
            .y +- epsilon
        )
        assert(
          GeoUtil
            .geoToCart(position4.latitude, position4.longitude)
            .z === GeoUtil
            .geoToCart(position5.latitude, position5.longitude)
            .z +- epsilon
        )
      }
    }
    "cartToGeo" should {
      "convert cartesian coords to lat/long" in {
        assert(GeoUtil.cartToGeo(cart1) === position1)
        assert(GeoUtil.cartToGeo(cart2) === position2)
        assert(GeoUtil.cartToGeo(cart4) === position4)
      }
      "converting from cart to geo and back should yield initial results" in {
        val pos = GeoUtil.cartToGeo(cart2)
        assert(GeoUtil.geoToCart(pos.latitude, pos.longitude) === cart2)
      }
    }
    "rangeFinalBearing" should {
      "should return the correct value" in {
        val inputs: List[Double] = List(0, 12, 360, -360, 348, -732, 180, -180)
        val outputs: List[Double] = List(180, 192, 180, 180, 168, 168, 0, 0)
        val results = new ListBuffer[Double]()
        for (input <- inputs) {
          results += GeoUtil.rangeFinalBearing(input)
        }
        assert(outputs === results.toList)
      }
    }
    "geoInitialBearing" should {
      "should return 0 when lat / long have not changed" in {
        assert(
          GeoUtil.geoInitialBearing(
            position1.latitude,
            position1.longitude,
            position1.latitude,
            position1.longitude
          ) == 0
        )

        assert(
          GeoUtil.geoInitialBearing(
            position2.latitude,
            position2.longitude,
            position2.latitude,
            position2.longitude
          ) == 0
        )
      }
      "should return the correct bearing" in {
        val bearing_from_1_to_2 = 90
        val bearing_from_2_to_1 = -90
        val bearing_from_1_to_4 = 0d
        val bearing_from_5_to_1 = 180d
        assert(
          GeoUtil.geoInitialBearing(
            position1.latitude,
            position1.longitude,
            position2.latitude,
            position2.longitude
          ) === bearing_from_1_to_2
        )
        assert(
          GeoUtil.geoInitialBearing(
            position2.latitude,
            position2.longitude,
            position1.latitude,
            position1.longitude
          ) === bearing_from_2_to_1
        )
        assert(
          GeoUtil.geoInitialBearing(
            position1.latitude,
            position1.longitude,
            position4.latitude,
            position4.longitude
          ) === bearing_from_1_to_4 +- epsilon
        )
        assert(
          GeoUtil.geoInitialBearing(
            89,
            0,
            position1.latitude,
            position1.longitude
          ) === bearing_from_5_to_1
        )
      }
      "should return the correct bearing when moving across the IDL" in {
        val pos1 = new Position("0", 25, 175)
        val pos2 = new Position("0", -12, -160)
        val bearing1 = 143.71609415266866
        val bearing2 = -33.25204906173284
        assert(
          GeoUtil.geoInitialBearing(
            pos1.latitude,
            pos1.longitude,
            pos2.latitude,
            pos2.longitude
          ) === bearing1
        )
        assert(
          GeoUtil.geoInitialBearing(
            pos2.latitude,
            pos2.longitude,
            pos1.latitude,
            pos1.longitude
          ) === bearing2
        )
      }
    }
    "geoDistance" should {
      val distance_1_to_2 = 556000d
      val distance_2_to_2 = 0d
      val distance_2_to_3 = 0d
      val distance_3_to_4 = 10018000d
      val distance_4_to_5 = 0d
      val distance_5_to_6 = 0d
      "calculate the correct distance given two Points (lat/long)" in {
        assert(
          GeoUtil.geoDistance(
            position1.latitude,
            position1.longitude,
            position2.latitude,
            position2.longitude
          ) === distance_1_to_2 +- distance_epsilon
        )
        assert(
          GeoUtil.geoDistance(
            position2.latitude,
            position2.longitude,
            position2.latitude,
            position2.longitude
          ) === distance_2_to_2 +- epsilon
        )
        assert(
          GeoUtil.geoDistance(
            position2.latitude,
            position2.longitude,
            position3.latitude,
            position3.longitude
          ) === distance_2_to_3 +- epsilon
        )
        assert(
          GeoUtil.geoDistance(
            position3.latitude,
            position3.longitude,
            position4.latitude,
            position4.longitude
          ) === distance_3_to_4 +- distance_epsilon
        )
      }
      "calculate the correct distance given two points across the IDL" in {
        val pos1 = new Position("0", 25, 175)
        val pos2 = new Position("0", -12, -160)
        val IDL_distance = 4932000d
        assert(
          GeoUtil.geoDistance(
            pos1.latitude,
            pos1.longitude,
            pos2.latitude,
            pos2.longitude
          ) === IDL_distance +- distance_epsilon
        )
      }
      "calculate the correct distance when both points are on the north pole" in {
        assert(
          GeoUtil.geoDistance(
            position4.latitude,
            position4.longitude,
            position5.latitude,
            position5.longitude
          ) === distance_4_to_5 +- epsilon
        )
        assert(
          GeoUtil.geoDistance(
            position5.latitude,
            position5.longitude,
            position6.latitude,
            position6.longitude
          ) === distance_5_to_6 +- epsilon
        )
      }
    }
    "geoMove" should {
      val time_constant = 1e9d

      "move the correct distance" in {
        val speed1 = 10000
        val brng1 = Bearing(45)
        val output1 = Position("0", 0.063611, 0.063611)

        val speed2 = 8712000
        val brng2 = Bearing(200)
        val output2 = Position("0", -64.0016, -36.8121)
        val move1 = GeoUtil.geoMove(
          position1.latitude,
          position1.longitude,
          speed1,
          brng1,
          time_constant
        )
        assert(move1.latitude === output1.latitude +- epsilon)
        assert(move1.longitude === output1.longitude +- epsilon)

        val move2 = GeoUtil.geoMove(5, 13, speed2, brng2, time_constant)
        assert(move2.latitude === output2.latitude +- epsilon)
        assert(move2.longitude === output2.longitude +- epsilon)
      }
      "move across the IDL" in {
        val speed = 4927000d
        val bearing = Bearing(143.7161d)
        val pos1 = new Position("0", 25, 175)
        val pos2 = new Position("0", -12, -160)
        val move = GeoUtil.geoMove(
          pos1.latitude,
          pos1.longitude,
          speed,
          bearing,
          time_constant
        )
        assert(move.latitude === pos2.latitude +- 0.1)
        assert(move.longitude === pos2.longitude +- 0.1)
      }
      "stay close to the north pole" in {
        val speed = 1d
        val move = GeoUtil.geoMove(
          position5.latitude,
          position5.longitude,
          speed,
          Bearing(5),
          time_constant
        )
        assert(move.latitude === position5.latitude +- epsilon)
      }
    }
  }
}
