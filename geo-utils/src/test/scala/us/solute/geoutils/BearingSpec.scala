package us.solute.geoutils

import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

class BearingSpec extends WordSpec with Matchers with BeforeAndAfterAll {

  val epsilon = 1e-4d

  val bearingList: List[Bearing] = List(
    Bearing(0d),
    Bearing(225.4325d),
    Bearing(12.234d),
    Bearing(375d),
    Bearing(225.4325d),
    Bearing(-390d)
  )

  val bearingOutput: List[Double] = List(
    0d,
    225.4325d,
    12.234d,
    375d,
    225.4325d,
    -390d
  )

  val relativeBearingOutput: List[Double] = List(
    0d,
    -134.5675d,
    12.234d,
    15d,
    -134.5675d,
    -30d
  )

  val bearingWrap360Output: List[Double] = List(
    0d,
    225.4325d,
    12.234d,
    15d,
    225.4325d,
    330d
  )

  "Bearing" should {
    "contain the degrees in the case class" in {
      for (i <- 0 to bearingList.length - 1) {
        assert(bearingList(i).degrees_0_to_360 == bearingOutput(i))
      }
    }
    "compare 2 different objects and return true if bearing values are the same" in {
      assert(bearingList(1) == bearingList(4))
    }
    "convert to relative bearing" in {
      for (i <- 0 to bearingList.length - 1) {
        assert(bearingList(i).relativeBearing === relativeBearingOutput(i) +- epsilon)
      }
    }
    "convert to radians" in {
      assert(bearingList(0).toRad == 0)
      assert(bearingList(1).toRad === 3.9345 +- epsilon)
      assert(bearingList(2).toRad === 0.2135 +- epsilon)
    }
    "convert relative bearing to radians" in {
      assert(bearingList(0).toRadRelativeBearing == 0)
      assert(bearingList(1).toRadRelativeBearing === -2.3486 +- epsilon)
      assert(bearingList(2).toRadRelativeBearing === 0.2135 +- epsilon)
    }
    "wrap the degrees to be between 0 and 360" in {
      for (i <- 0 to bearingList.length - 1) {
        assert(bearingList(i).wrap360 === bearingWrap360Output(i) +- epsilon)
      }
    }
  }
}
