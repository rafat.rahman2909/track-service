package us.solute.smootherservice.stream.api

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import us.solute.smootherservice.api.SmoothedAOU

/**
  * The Smoother Service stream interface.
  *
  * This describes everything that Lagom needs to know about how to serve and
  * consume the SmootherServiceStream service.
  */
trait SmootherServiceStream extends Service {

  def smootherStream: ServiceCall[NotUsed, Source[SmoothedAOU, NotUsed]]

  override final def descriptor: Descriptor = {
    import Service._

    named("smoother-service-stream")
      .withCalls(
        namedCall("smootherstream", smootherStream _)
      ).withAutoAcl(true)
  }
}

