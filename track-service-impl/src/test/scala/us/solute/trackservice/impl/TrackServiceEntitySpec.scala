package us.solute.trackservice.impl

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.lightbend.lagom.scaladsl.playjson.JsonSerializerRegistry
import com.lightbend.lagom.scaladsl.testkit.PersistentEntityTestDriver
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import us.solute.trackservice.api.{Position, TrackUpdate}

class TrackServiceEntitySpec
    extends WordSpec
    with Matchers
    with BeforeAndAfterAll {

  private val system = ActorSystem(
    "TrackServiceEntitySpec",
    JsonSerializerRegistry.actorSystemSetupFor(TrackServiceSerializerRegistry)
  )

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  private def withTestDriver(
    block: PersistentEntityTestDriver[TrackServiceCommand[_],
                                      TrackServiceEvent,
                                      TrackServiceState] => Unit
  ): Unit = {
    val driver = new PersistentEntityTestDriver(
      system,
      new TrackServiceEntity,
      "track-service-1"
    )
    block(driver)
    driver.getAllIssues should have size 0
  }

  "Track Service entity" should {

    "Update State with new TrackUpdate" in withTestDriver { driver =>
      val defaultPos: Float = 0.0.toFloat
      val position = new Position("alt", defaultPos, defaultPos)
      val trackUpdate = new TrackUpdate("testTrack1", "code", position)
      val outcome = driver.run(UpdateTrack(trackUpdate))
      outcome.state.lastUpdate should ===(Some(trackUpdate))
    }
  }
}
