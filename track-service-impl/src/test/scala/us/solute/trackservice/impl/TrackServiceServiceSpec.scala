//package us.solute.trackservice.impl
//
//import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
//import com.lightbend.lagom.scaladsl.testkit.ServiceTest
//import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}
//import us.solute.trackservice.api._
//
//class TrackServiceServiceSpec extends AsyncWordSpec with Matchers with BeforeAndAfterAll {
//
//  private val server = ServiceTest.startServer(
//    ServiceTest.defaultSetup
//      .withCassandra()
//  ) { ctx =>
//    new TrackServiceApplication(ctx) with LocalServiceLocator
//  }
//
//  val client: TrackService = server.serviceClient.implement[TrackService]
//
//  override protected def afterAll(): Unit = server.stop()
//
//  "Track Service service" should {
//
//    "say hello" in {
//      client.hello("Alice").invoke().map { answer =>
//        answer should ===("Hello, Alice!")
//      }
//    }
//
//    "allow responding with a custom message" in {
//      for {
//        _ <- client.useGreeting("Bob").invoke(GreetingMessage("Hi"))
//        answer <- client.hello("Bob").invoke()
//      } yield {
//        answer should ===("Hi, Bob!")
//      }
//    }
//  }
//}
