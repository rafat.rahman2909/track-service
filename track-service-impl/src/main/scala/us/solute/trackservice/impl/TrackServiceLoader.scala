package us.solute.trackservice.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.trackservice.api.TrackService
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.playjson.JsonSerializerRegistry
import com.softwaremill.macwire._
import play.filters.cors.CORSComponents
import play.api.mvc.EssentialFilter

class TrackServiceLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TrackServiceApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TrackServiceApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[TrackService])
}

abstract class TrackServiceApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with CassandraPersistenceComponents
    with LagomKafkaComponents
    with AhcWSComponents
    with CORSComponents {
  override val httpFilters: Seq[EssentialFilter] = Seq(corsFilter)
//
  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[TrackService](wire[TrackServiceImpl])

  // Register the JSON serializer registry
  override lazy val jsonSerializerRegistry: JsonSerializerRegistry = TrackServiceSerializerRegistry

  // Register the Track Service persistent entity
  persistentEntityRegistry.register(wire[TrackServiceEntity])
}
