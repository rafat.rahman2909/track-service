package us.solute.trackservice.impl

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import akka.{Done}
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.AggregateEventTag
import com.lightbend.lagom.scaladsl.persistence.ReadSideProcessor
import com.lightbend.lagom.scaladsl.persistence.ReadSideProcessor.ReadSideHandler
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraReadSide
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraSession
import us.solute.trackservice.api.{Position, TrackUpdate}

class TrackServiceEventProcessor(session: CassandraSession, readSide: CassandraReadSide)(implicit ec: ExecutionContext)
  extends ReadSideProcessor[TrackServiceEvent] {

  val createTableStatement = """CREATE TABLE IF NOT EXISTS trackupdate (
  id TEXT,
  code TEXT,
  alt TEXT,
  lat DOUBLE,
  lon DOUBLE,
  time TEXT,
  PRIMARY KEY (id)
)
"""

  // Writes
  val updateTrackStatement = """INSERT INTO trackupdate (id, code, alt, lat, lon, time) VALUES (?, ?, ?, ?, ?, ?)"""
  var updateTrack: PreparedStatement = _

  // Reads
  val getTrackUpdatesStatement = """SELECT * FROM trackupdate"""
  var getTrackUpdatesPrepared: PreparedStatement = _

  def getTrackUpdates(): Future[List[TrackUpdate]] = {
    session.selectAll(getTrackUpdatesPrepared.bind())
      .map(rows => rows.map(row => TrackUpdate(row.getString("id"), row.getString("code"),
      Position(row.getString("alt"), row.getDouble("lat"), row.getDouble("lon")),
        row.getString("time"))).toList)
  }

  def bindTrackUpdated(trackUpdate: TrackUpdate): BoundStatement = {
    val boundStatement = updateTrack.bind()
    boundStatement.setString("id", trackUpdate.id)
    boundStatement.setString("code", trackUpdate.code)
    boundStatement.setString("alt", trackUpdate.position.alt)
    boundStatement.setDouble("lat", trackUpdate.position.latitude)
    boundStatement.setDouble("lon", trackUpdate.position.longitude)
    boundStatement.setString("time", trackUpdate.time)
    boundStatement
  }

  def processTrackUpdate(event: TrackUpdated): Future[List[BoundStatement]] = {
    Future.successful(List(bindTrackUpdated(event.track)))
  }

  override def buildHandler(): ReadSideHandler[TrackServiceEvent] = readSide.builder[TrackServiceEvent]("trackupdateoffset")
    .setGlobalPrepare(() =>
      for {
        _ <- session.executeCreateTable(createTableStatement)
      } yield Done)
    .setPrepare(_ =>
      for {
        _updateTrack <- session.prepare(updateTrackStatement)
        _getTrackUpdates <- session.prepare(getTrackUpdatesStatement)
      } yield {
        updateTrack = _updateTrack
        getTrackUpdatesPrepared = _getTrackUpdates
        Done
      })
    .setEventHandler[TrackUpdated](e => processTrackUpdate(e.event))
    .build()

  override def aggregateTags: Set[AggregateEventTag[TrackServiceEvent]] = TrackServiceEvent.Tag.allTags
}
