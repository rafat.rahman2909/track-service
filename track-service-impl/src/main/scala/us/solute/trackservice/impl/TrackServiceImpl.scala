package us.solute.trackservice.impl

import us.solute.trackservice.api.{Position, TrackList, TrackService, TrackSet, TrackUpdate}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}

import scala.concurrent.{ExecutionContext}
/**
  * Implementation of the TrackService.
  */
class TrackServiceImpl(persistentEntityRegistry: PersistentEntityRegistry,
                       readSide: ReadSide,
                       cassandraReadSide: CassandraReadSide,
                       cassandraSession: CassandraSession)(implicit ec: ExecutionContext) extends TrackService {


  val eventProcessor = new TrackServiceEventProcessor(cassandraSession, cassandraReadSide)

  readSide.register[TrackServiceEvent](eventProcessor)

  override def track(id: String) = ServiceCall { _ =>
    // Look up the Track Service entity for the given ID.
    val ref = persistentEntityRegistry.refFor[TrackServiceEntity](id)
    // Ask the entity the Hello command.
    ref.ask(GetTrack)
  }

  override def tracks() = ServiceCall { _ =>
    eventProcessor.getTrackUpdates()
  }

  override def updateTrack() = ServiceCall { track =>
    // Look up the Track Service entity for the given ID.
    val ref = persistentEntityRegistry.refFor[TrackServiceEntity](track.id)
    // Tell the entity to use the greeting message specified.
    ref.ask(UpdateTrack(track: TrackUpdate))
  }

  override def tracksTopic(): Topic[TrackUpdate] = {
    TopicProducer.taggedStreamWithOffset(TrackServiceEvent.Tag) { (eventStream, offset) =>
      persistentEntityRegistry.eventStream(eventStream, offset).map(ev => (convertEvent(ev), ev.offset))
    }
  }

  private def convertEvent(trackEvent: EventStreamElement[TrackServiceEvent]): TrackUpdate = {
    trackEvent.event match {
      case TrackUpdated(trackUpdate) => trackUpdate
    }
  }
}
