package us.solute.trackservice.impl

import java.time.LocalDateTime

import akka.Done
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventShards, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import play.api.libs.json.{Format, JsSuccess, Json, Reads, Writes}
import us.solute.trackservice.api.TrackUpdate

import scala.collection.immutable.Seq

/**
  * This is an event sourced entity. It has a state, [[TrackServiceState]], which
  * stores what the greeting should be (eg, "Hello").
  *
  * Event sourced entities are interacted with by sending them commands. This
  * entity supports two commands, a [[UseGreetingMessage]] command, which is
  * used to change the greeting, and a [[Hello]] command, which is a read
  * only command which returns a greeting to the name specified by the command.
  *
  * Commands get translated to events, and it's the events that get persisted by
  * the entity. Each event will have an event handler registered for it, and an
  * event handler simply applies an event to the current state. This will be done
  * when the event is first created, and it will also be done when the entity is
  * loaded from the database - each event will be replayed to recreate the state
  * of the entity.
  *
  * This entity defines one event, the [[GreetingMessageChanged]] event,
  * which is emitted when a [[UseGreetingMessage]] command is received.
  */
class TrackServiceEntity extends PersistentEntity {

  override type Command = TrackServiceCommand[_]
  override type Event = TrackServiceEvent
  override type State = TrackServiceState

  /**
    * The initial state. This is used if there is no snapshotted state to be found.
    */
  override def initialState: TrackServiceState = TrackServiceState(None, LocalDateTime.now.toString)

  /**
    * An entity can define different behaviours for different states, so the behaviour
    * is a function of the current state to a set of actions.
    */
  override def behavior: Behavior = {
    case TrackServiceState(messages, _) => Actions().onCommand[UpdateTrack, Done] {

      // Command handler for the UseGreetingMessage command
      case (UpdateTrack(track), ctx, state) =>
        // In response to this command, we want to first persist it as a
        // TrackUpdated event
        ctx.thenPersist(
          TrackUpdated(track)
        ) { _ =>
          // Then once the event is successfully persisted, we respond with done.
          ctx.reply(Done)
        }

    }.onReadOnlyCommand[GetTrack.type, TrackUpdate] {

      // Command handler for the Hello command
      case (GetTrack, ctx, state) =>
        // Reply with a message built from the current message, and the name of
        // the person we're meant to say hello to.
        ctx.reply(state.lastUpdate getOrElse null)

    }.onEvent {

      // Event handler for the GreetingMessageChanged event
      case (TrackUpdated(track), state) =>
        // We simply update the current state to use the greeting message from
        // the event.
        state.updateTrack(track)
    }
  }
}

/**
  * The current state held by the persistent entity.
  */
case class TrackServiceState(lastUpdate: Option[TrackUpdate], timestamp: String) {
  def updateTrack(track: TrackUpdate) = {
    copy(lastUpdate = Some(track), timestamp = LocalDateTime.now().toString)
  }
}

object TrackServiceState {
  /**
    * Format for the hello state.
    *
    * Persisted entities get snapshotted every configured number of events. This
    * means the state gets stored to the database, so that when the entity gets
    * loaded, you don't need to replay all the events, just the ones since the
    * snapshot. Hence, a JSON format needs to be declared so that it can be
    * serialized and deserialized when storing to and from the database.
    */
  implicit val format: Format[TrackServiceState] = Json.format
}

/**
  * This interface defines all the events that the TrackServiceEntity supports.
  */
sealed trait TrackServiceEvent extends AggregateEvent[TrackServiceEvent] {
  def aggregateTag: AggregateEventShards[TrackServiceEvent] = TrackServiceEvent.Tag
}

object TrackServiceEvent {
  val NumShards = 20
  val Tag = AggregateEventTag.sharded[TrackServiceEvent](NumShards)
}

/**
  * An event that represents a change in greeting message.
  */
case class TrackUpdated(track: TrackUpdate) extends TrackServiceEvent

object TrackUpdated {

  /**
    * Format for the greeting message changed event.
    *
    * Events get stored and loaded from the database, hence a JSON format
    * needs to be declared so that they can be serialized and deserialized.
    */
  implicit val format: Format[TrackUpdated] = Json.format
}

/**
  * This interface defines all the commands that the TrackServiceEntity supports.
  */
sealed trait TrackServiceCommand[R] extends ReplyType[R]

/**
  * A command to switch the greeting message.
  *
  * It has a reply type of [[Done]], which is sent back to the caller
  * when all the events emitted by this command are successfully persisted.
  */
case class UpdateTrack(track: TrackUpdate) extends TrackServiceCommand[Done]

object UpdateTrack {

  /**
    * Format for the use greeting message command.
    *
    * Persistent entities get sharded across the cluster. This means commands
    * may be sent over the network to the node where the entity lives if the
    * entity is not on the same node that the command was issued from. To do
    * that, a JSON format needs to be declared so the command can be serialized
    * and deserialized.
    */
  implicit val format: Format[UpdateTrack] = Json.format
}

case object GetTrack  extends TrackServiceCommand[TrackUpdate] {

  /**
    * Format for the hello command.
    *
    * Persistent entities get sharded across the cluster. This means commands
    * may be sent over the network to the node where the entity lives if the
    * entity is not on the same node that the command was issued from. To do
    * that, a JSON format needs to be declared so the command can be serialized
    * and deserialized.
    */
  implicit val format: Format[GetTrack.type] = Json.format
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader.
  */
object TrackServiceSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[UpdateTrack],
    JsonSerializer[TrackUpdated],
    JsonSerializer[GetTrack.type ],
    JsonSerializer[TrackServiceState]
  )
}
