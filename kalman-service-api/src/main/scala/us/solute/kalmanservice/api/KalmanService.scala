package us.solute.kalmanservice.api

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}
import play.api.libs.json.{Format, Json}
import us.solute.sensor.service.api.Ellipse
import us.solute.trackservice.api.Position

object KalmanService  {
  val TOPIC_NAME = "kalman"
}

trait KalmanService extends Service {

  /**
    * Example: curl http://localhost:9000/api/kalmanAou/0
    */
  def kalmanAou(id: String): ServiceCall[NotUsed, KalmanAOU]


  /**
   * Example: curl http://localhost:9000/api/kalmanAou
   */
  def kalmanAous(): ServiceCall[NotUsed, List[KalmanAOU]]


  /**
    * Example: curl http://localhost:9000/api/kalmanAou
    */
  def updateKalmanAOU(): ServiceCall[KalmanAOU, Done]

  /**
    * Example: curl http://localhost:9000/api/kalmanaou/start
    */
  def startKalmanAOU(): ServiceCall[KalmanInput, Done]

  /**
    * This gets published to Kafka.
    */
  def kalmanTopic(): Topic[KalmanAOU]

  override final def descriptor: Descriptor = {
    import Service._
    // @formatter:off
    named("kalman-service")
      .withCalls(
        pathCall("/api/kalmanaou/:id", kalmanAou _),
        pathCall("/api/kalmanaou", updateKalmanAOU _),
        pathCall("/api/kalmanaou", kalmanAous _),
        pathCall("/api/kalmanaou/start", startKalmanAOU _),
      )
      .withTopics(
        topic(KalmanService.TOPIC_NAME, kalmanTopic _)
      )
      .withAutoAcl(true)
    // @formatter:on
  }

}

case class KalmanInput(iterations: Double)
object KalmanInput {
  implicit val format: Format[KalmanInput] = Json.format[KalmanInput]
}

case class KalmanAOU(id: String, x_k: String, p_k: String)
object KalmanAOU {
  implicit val format: Format[KalmanAOU] = Json.format[KalmanAOU]
}