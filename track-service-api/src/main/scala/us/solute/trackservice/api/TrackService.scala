package us.solute.trackservice.api

import akka.stream.scaladsl.Source
import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.Service.pathCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceAcl, ServiceCall}
import play.api.libs.json.{Format, Json}

object TrackService  {
  val TOPIC_NAME = "tracks"
}

/**
  * The Track Service service interface.
  * <p>
  * This describes everything that Lagom needs to know about how to serve and
  * consume the TrackServiceService.
  */
trait TrackService extends Service {


  /**
    * Example: curl http://localhost:9000/api/tracks
    */
  def tracks(): ServiceCall[NotUsed, List[TrackUpdate]]

  /**
    * Example: curl http://localhost:9000/api/tracks/0
    */
  def track(id: String): ServiceCall[NotUsed, TrackUpdate]

  /**
    * Example: curl -H "Content-Type: application/json" -X POST -d '{"id":
    * "Hi"}' http://localhost:9000/api/tracks
    */
  def updateTrack(): ServiceCall[TrackUpdate, Done]

  /**
    * This gets published to Kafka.
    */
  def tracksTopic(): Topic[TrackUpdate]

  override final def descriptor: Descriptor = {
    import Service._
    // @formatter:off
    named("track-service")
      .withCalls(
        pathCall("/api/tracks", tracks _),
        pathCall("/api/tracks/:id", track _),
        pathCall("/api/tracks", updateTrack _),
      )
      .withTopics(
        topic(TrackService.TOPIC_NAME, tracksTopic _)
      )
      .withAutoAcl(true)

  }
}

case class Cart(x: Double, y:Double, z:Double)
object Cart {
  implicit val format: Format[Cart] = Json.format[Cart]
}

case class Position(alt: String, latitude: Double, longitude: Double)
object Position {
  implicit val format: Format[Position] = Json.format[Position]
}
case class TrackUpdate(id: String, code: String, position: Position, time: String)
object TrackUpdate {
  implicit val format: Format[TrackUpdate] = Json.format[TrackUpdate]
}

case class TrackSet(tracks: Map[String, TrackUpdate])

object TrackSet {
  implicit val format: Format[TrackSet] = Json.format[TrackSet]
}
case class TrackList(tracks: List[TrackUpdate])

object TrackList {
  implicit val format: Format[TrackList] = Json.format[TrackList]
}
