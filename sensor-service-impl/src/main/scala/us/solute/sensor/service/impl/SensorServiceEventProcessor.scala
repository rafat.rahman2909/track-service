package us.solute.sensor.service.impl

import akka.Done
import com.datastax.driver.core.{BoundStatement, PreparedStatement}
import com.lightbend.lagom.scaladsl.persistence.{AggregateEventTag, ReadSideProcessor}
import com.lightbend.lagom.scaladsl.persistence.ReadSideProcessor.ReadSideHandler
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import us.solute.sensor.service.api.{AOUUpdate, Ellipse}
import us.solute.trackservice.api.Position

import scala.concurrent.{ExecutionContext, Future}

class SensorServiceEventProcessor(session: CassandraSession, readSide: CassandraReadSide)(implicit ec: ExecutionContext)
  extends ReadSideProcessor[SensorServiceEvent] {

  val createTableStatement = """CREATE TABLE IF NOT EXISTS aouupdate (
  id TEXT,
  alt TEXT,
  lat DOUBLE,
  lon DOUBLE,
  smaj DOUBLE,
  smin DOUBLE,
  bearing DOUBLE,
  trackLat DOUBLE,
  trackLong DOUBLE,
  time TEXT,
  PRIMARY KEY (id)
)
"""
  // Writes
  val updateAOUStatement = """INSERT INTO aouupdate (id, alt, lat, lon, smaj, smin, bearing, trackLat, trackLong, time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
  var updateAOU: PreparedStatement = _
  // Reads
  val getAOUUpdatesStatement = """SELECT * FROM aouupdate"""
  var getAOUUpdatesPrepared: PreparedStatement = _

  def getAOUUpdates(): Future[List[AOUUpdate]] = {
    session.selectAll(getAOUUpdatesPrepared.bind())
      .map(rows => rows.map(row => AOUUpdate(row.getString("id"), Position(row.getString("alt"),
        row.getDouble("lat"), row.getDouble("lon")), Ellipse(row.getDouble("smaj"), row.getDouble("smin"),
          row.getDouble("bearing")), row.getDouble("trackLat"), row.getDouble("trackLong"), row.getString("time") )).toList)
  }

  def bindAOUUpdated(aouUpdate: AOUUpdate): BoundStatement = {
    val boundStatement = updateAOU.bind()
    boundStatement.setString("id", aouUpdate.id)
    boundStatement.setString("alt", aouUpdate.centerPt.alt)
    boundStatement.setDouble("lat", aouUpdate.centerPt.latitude)
    boundStatement.setDouble("lon", aouUpdate.centerPt.longitude)
    boundStatement.setDouble("smaj", aouUpdate.ellipse.smaj)
    boundStatement.setDouble("smin", aouUpdate.ellipse.smin)
    boundStatement.setDouble("bearing", aouUpdate.ellipse.bearing)
    boundStatement.setDouble("trackLat", aouUpdate.trackLat)
    boundStatement.setDouble("trackLong", aouUpdate.trackLong)
    boundStatement.setString("time", aouUpdate.time)
    boundStatement
  }

  def processAOUUpdate(event: AOUUpdated): Future[List[BoundStatement]] = {
    Future.successful(List(bindAOUUpdated(event.aou)))
  }

  override def buildHandler(): ReadSideHandler[SensorServiceEvent] = readSide.builder[SensorServiceEvent]("aouupdateoffset")
    .setGlobalPrepare(() =>
      for {
        _ <- session.executeCreateTable(createTableStatement)
      } yield Done)
    .setPrepare(_ =>
      for {
        _updateAOU <- session.prepare(updateAOUStatement)
        _getAOUUpdates <- session.prepare(getAOUUpdatesStatement)
      } yield {
        updateAOU = _updateAOU
        getAOUUpdatesPrepared = _getAOUUpdates
        Done
      })
    .setEventHandler[AOUUpdated](e => processAOUUpdate(e.event))
    .build()

  override def aggregateTags: Set[AggregateEventTag[SensorServiceEvent]] = SensorServiceEvent.Tag.allTags
}
