package us.solute.sensor.service.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.broker.kafka.{LagomKafkaClientComponents, LagomKafkaComponents}
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.sensor.service.api.SensorService
import us.solute.trackservice.api.TrackService

class SensorServiceLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TrackServiceSensorApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TrackServiceSensorApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[SensorService])
}

abstract class TrackServiceSensorApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with ClusterComponents
    with AhcWSComponents
    with LagomKafkaComponents
    with CassandraPersistenceComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[SensorService](wire[SensorServiceImpl])

  override lazy val jsonSerializerRegistry = SensorServiceSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]

  // Register the Sensor Service Simulator persistent entity
  persistentEntityRegistry.register(wire[SensorServiceEntity])
}

