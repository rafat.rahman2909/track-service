package us.solute.sensor.service.impl

import akka.Done
import akka.stream.scaladsl.Flow
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.cassandra.{CassandraReadSide, CassandraSession}
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry, ReadSide}
import us.solute.sensor.service.api
import us.solute.sensor.service.api.SensorService
import us.solute.trackservice.api.{Position, TrackService}
import us.solute.sensor.service.api.{AOUUpdate, Bias, Ellipse}
import scala.math.{sqrt, log}
import org.apache.commons.math3.distribution.NormalDistribution

import scala.concurrent.ExecutionContext

import scala.concurrent.Future
import scala.util.Random

class SensorServiceImpl(trackService: TrackService, persistentEntityRegistry: PersistentEntityRegistry, readSide: ReadSide,
cassandraReadSide: CassandraReadSide,
cassandraSession: CassandraSession)(implicit ec: ExecutionContext) extends SensorService {

  val eventProcessor = new SensorServiceEventProcessor(cassandraSession, cassandraReadSide)

  readSide.register[SensorServiceEvent](eventProcessor)

  private var AOURequested = false
  private var timeOnLeg = 0d
  private var sensorEventTime = 0d
  private var timeWarp = 1d
  private var time = System.nanoTime()
  private var remainingTimeOnLeg = 0d
  private var remainingSensorEventTime = 0d
  private var sminBias = Bias(0,0,0)
  private var smajBias = Bias(0,0,0)
  private var bearingBias = Bias(0,0,0)
  private var centerPtBias = Bias(0,0,0)

  trackService.tracksTopic().subscribe.atLeastOnce(
    Flow.fromFunction(trackUpdate => {
      val ref = persistentEntityRegistry.refFor[SensorServiceEntity](trackUpdate.id)
      if (AOURequested) {
        val currentTime = System.nanoTime()
        val delta_s = timeWarp * (currentTime-time)
        remainingTimeOnLeg = remainingTimeOnLeg - (delta_s/1e9)
        if(remainingTimeOnLeg < 0) {
          sminBias = Bias(sminBias.mean + (Random.nextDouble()/1e6).toDouble, sminBias.percentDistribution, sminBias.distWidth)
          smajBias = Bias(smajBias.mean + (Random.nextDouble()/1e6).toDouble, smajBias.percentDistribution, smajBias.distWidth)
          bearingBias = Bias(bearingBias.mean + Random.nextDouble(), bearingBias.percentDistribution, bearingBias.distWidth)
          centerPtBias = Bias(centerPtBias.mean + (Random.nextDouble()/1e6).toDouble, centerPtBias.percentDistribution, centerPtBias.distWidth)
          remainingTimeOnLeg = Random.nextDouble() * timeOnLeg
        }
        remainingSensorEventTime = remainingSensorEventTime - (delta_s/1e9)
        if(remainingSensorEventTime < 0) {
          remainingSensorEventTime = sensorEventTime
          val newAOU = makeRandomAOU(trackUpdate.position.latitude, trackUpdate.position.longitude, trackUpdate.id)
          ref.ask(UpdateAOU(newAOU: AOUUpdate))
        }
        time = currentTime
      }
      Done
    }))

  override def aou(id: String) = ServiceCall { _ =>
    // Look up the Sensor Service entity for the given ID.
    val ref = persistentEntityRegistry.refFor[SensorServiceEntity](id)
    // Ask the entity the Hello command.
    ref.ask(GetAOU)
  }
  override def aouTopic(): Topic[AOUUpdate] = {
    TopicProducer.taggedStreamWithOffset(SensorServiceEvent.Tag) { (eventStream, offset) =>
      persistentEntityRegistry.eventStream(eventStream, offset).map(ev => (convertEvent(ev), ev.offset))
    }
  }

  private def convertEvent(trackEvent: EventStreamElement[SensorServiceEvent]): AOUUpdate = {
    trackEvent.event match {
      case AOUUpdated(aouUpdate) => aouUpdate
    }
  }

  /*
  @input lat: Double latitude of track
  @input long: Double longitude of track
  @input id: Track ID
  @output AOUUpdate: Simulated sensor AOU based on random gaussian values
   */
  private def makeRandomAOU(lat: Double, long: Double, id: String): AOUUpdate = {
    val randomCenter = Position("0", lat + randomGaussian(centerPtBias.mean,
      calculateSTDDev(centerPtBias.distWidth, centerPtBias.percentDistribution)), long +
      randomGaussian(centerPtBias.mean, calculateSTDDev(centerPtBias.distWidth,
        centerPtBias.percentDistribution)))
    val ellipse = makeRandomEllipse()
    val newAOU = api.AOUUpdate(id, randomCenter, ellipse, lat, long, System.nanoTime().toString())
    newAOU
  }

  /*
  @output Ellipse: Random created ellipse with semi random major axis, semi random minor axis and semi random bearing
   */
  private def makeRandomEllipse(): Ellipse = {
    val smaj = Math.abs(randomGaussian(smajBias.mean, calculateSTDDev(smajBias.distWidth, smajBias.percentDistribution)))
    val smin = Math.abs(randomGaussian(sminBias.mean, calculateSTDDev(sminBias.distWidth, sminBias.percentDistribution)))
    val bearing = randomGaussian(bearingBias.mean, calculateSTDDev(bearingBias.distWidth, bearingBias.percentDistribution))
    Ellipse(smaj.toDouble, smin.toDouble, bearing.toDouble)
  }

  /*
  @input biasWidth: Double width of the gaussian bias
  @input percent: Double input for percent containment, ie percent for gaussian distribution
  @output: standard deviation of the distribution for box muller
   */
  private def calculateSTDDev(biasWidth: Double, percent: Double): Double = {
    val standardNormal = new NormalDistribution(0, 1);
    val zscore = standardNormal.inverseCumulativeProbability(percent/100)
    // Standard deviation
    biasWidth/zscore
  }

  /*
  @input mean: Double mean of the distribution
  @input stdDev: Double standard deviation of the distribution
  @output Double a pseudorandom sample from a gaussian distribution with mean and stdDev
  more info can be found here https://www.javamex.com/tutorials/random_numbers/gaussian_distribution_2.shtml
   */
  private def randomGaussian(mean: Double, stdDev: Double): Double = {
    Random.nextGaussian() * stdDev + mean
  }

  override def updateAOU ()= ServiceCall { aou =>
    // Look up the Track Service Sensor entity for the given ID.
    val ref = persistentEntityRegistry.refFor[SensorServiceEntity](aou.id)
    // Tell the entity to use the greeting message specified.
    ref.ask(UpdateAOU(aou: AOUUpdate))
  }

  /**
    * Example: curl http://localhost:9000/api/aou/start
    */
  override def startAOU() = ServiceCall { userInput =>
    AOURequested = true
    time = System.nanoTime()
    timeOnLeg = Random.nextDouble() * userInput.timeOnLeg
    remainingTimeOnLeg = timeOnLeg
    sensorEventTime = userInput.sensorEventTime
    remainingSensorEventTime = userInput.sensorEventTime
    timeWarp = userInput.timeWarp
    sminBias = userInput.sminBias
    smajBias = userInput.smajBias
    bearingBias = userInput.bearingBias
    centerPtBias = userInput.centerPtBias
    Future.successful(Done)
  }

  /**
   * Example: curl http://localhost:9000/api/aou
   */
  override def aous()= ServiceCall { _ =>
    eventProcessor.getAOUUpdates()
  }
}
