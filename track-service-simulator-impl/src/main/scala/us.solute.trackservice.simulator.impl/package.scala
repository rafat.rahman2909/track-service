package us.solute.trackservice.simulator.impl

import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import us.solute.trackservice.api.Position
import us.solute.trackservice.simulator.api.{BoundingBox, SimulatorProps}

import scala.collection.immutable.Seq

package object Track {
  import play.api.libs.json.Format
  import play.api.libs.json.Json

  case object SimulateTrack {
    implicit val format: Format[SimulateTrack.type] = Json.format
  }

  case object StopSimulation {
    implicit val format: Format[StopSimulation.type] = Json.format
  }

  case object StartSimulation {
    implicit val format: Format[StartSimulation.type] = Json.format
  }
}

object TrackServiceSimulatorSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[SimulatorProps],
    JsonSerializer[BoundingBox],
    JsonSerializer[Position],
    JsonSerializer[Track.SimulateTrack.type],
    JsonSerializer[Track.StartSimulation.type],
    JsonSerializer[Track.StopSimulation.type]
  )
}