package us.solute.trackservice.simulator.impl

import akka.actor.{Actor, Cancellable, PoisonPill, Props}
import us.solute.trackservice.api.{TrackService, TrackUpdate, Position => Pos}
import us.solute.trackservice.simulator.api.{BoundingBox, Position, Physics}
import us.solute.trackservice.simulator.impl.Track.{SimulateTrack, StartSimulation, StopSimulation}

import scala.util.Random
import scala.concurrent.duration._

object TrackActor {
  def props = Props[TrackActor]
  private val earth_R = 6378.1 * 1000

  /*
  @input lat, lng: Doubles for position of starting point
  @input speed: Double for speed we want to move the point
  @input brng: Double for angle we want to move the point at
  @input time: Double for time, used for calculating distance based on speed and time
  @output New calculated position accounting for earth's geo
   */
  def geoMove(lat: Double, lng: Double, speed: Double, brng: Double, time: Double)= {

    val distance = (speed * time)/(1e9)
    val lat1 = Math.asin(Math.sin(lat) * Math.cos(distance / earth_R) +
      Math.cos(lat) * Math.sin(distance / earth_R) * Math.cos(brng));
    val lng1 = lng + Math.atan2(Math.sin(brng) * Math.sin(distance / earth_R) * Math.cos(lat),
      Math.cos(distance / earth_R) - Math.sin(lat) * Math.sin(lat1));

    Math.toDegrees(lat1)
    Math.toDegrees(lng1)
    (lat1, lng1)
  }

  /*
  @input boundingBox: Two positions we want to bound our track in
  @output a random position lat/long
   */
  def randomPosition(boundingBox: BoundingBox) = {
    val BoundingBox(Position(lat1, lon1), Position(lat2, lon2)) = boundingBox;
    (lat1 + (Random.nextDouble() * distanceBetween(lat1, lat2)),
      lon1 + (Random.nextDouble() * distanceBetween(lon1, lon2)))
  }

  /*
  @input f1, f2: Doubles to find the distance between them
  @output Double valued at distance between two input points
   */
  def distanceBetween(f1: Double, f2: Double): Double = {
    if((f1 < 0 && f2 < 0) || f1 > 0 && f2 > 0) {
      f1 + f2
    } else {
     Math.abs(f1) + Math.abs(f2)
    }
  }
}

class TrackActor(trackService: TrackService, trackId: String, boundingBox: BoundingBox, physics: Physics,
                 delay: FiniteDuration, period: FiniteDuration, duration: FiniteDuration) extends Actor {

  override def preStart(): Unit = {
    self ! StartSimulation
  }

  override def postStop(): Unit = {
    self ! StopSimulation
  }

  override def receive: Receive = {

    case StartSimulation => {
      val system = context.system
      import system.dispatcher
      val cancellable = system.scheduler.schedule(delay, period, self, SimulateTrack)
      system.scheduler.scheduleOnce(duration, self, PoisonPill)
      val speed = physics.speed//Random.nextDouble() * physics.speed
      val bearing = physics.bearing//Random.nextDouble() % physics.bearing
      val time = System.nanoTime()
      val timeOnLeg = Random.nextDouble() * physics.timeOnLeg
      val (lat, lon) = TrackActor.randomPosition(boundingBox)
      context become updated(lat.toDouble, lon.toDouble, speed, bearing, time, timeOnLeg, cancellable)
    }
  }

  /*
  @input currentLat, currentLon: Doubles, current position in lat/long form
  @input speed: Double for current speed
  @input bearing: Double for current bearing
  @input time: Double for current time
  @input timeOnLeg: Double for current time on leg, after time has been reached we want to have new random positions for
  a true random walk
   */
  private def updated(currentLat: Double, currentLon: Double, speed: Double, bearing: Double, time: Double,
                      timeOnLeg: Double, cancellable: Cancellable): Receive = {
    case SimulateTrack => {
      val currentTime = System.nanoTime()
      val delta_s = physics.timeWarp*(currentTime-time)
      var remainingTimeOnLeg = timeOnLeg - (delta_s/1e9)

      val (lat, lng) = TrackActor.geoMove(currentLat, currentLon, speed, bearing, delta_s)
      trackService.updateTrack().invoke(TrackUpdate(trackId, "SPZ------------", Pos("0", lat.toDouble, lng.toDouble),
        (delta_s/1e9).toString))
      //println(physics.speed)
      if(remainingTimeOnLeg < 0){
        remainingTimeOnLeg = Random.nextDouble() * physics.timeOnLeg
        val newspeed = physics.speed//Random.nextDouble() * physics.speed
        val newbearing = physics.bearing//Random.nextDouble() % physics.bearing
        context become updated(lat.toDouble, lng.toDouble, newspeed, newbearing, currentTime, remainingTimeOnLeg, cancellable)
      } else {
        context become updated(lat.toDouble, lng.toDouble, speed, bearing, currentTime, remainingTimeOnLeg, cancellable)
      }
    }
    case StopSimulation => cancellable.cancel()
  }
}
