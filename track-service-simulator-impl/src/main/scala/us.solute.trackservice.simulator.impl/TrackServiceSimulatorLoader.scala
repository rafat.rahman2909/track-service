package us.solute.trackservice.simulator.impl

import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaClientComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import play.api.libs.ws.ahc.AhcWSComponents
import us.solute.trackservice.simulator.api.{TrackServiceSimulator}
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import us.solute.trackservice.api.{TrackService}

import scala.collection.immutable.Seq

class TrackServiceSimulatorLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TrackServiceSimulatorApplication(context) with AkkaDiscoveryComponents

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TrackServiceSimulatorApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[TrackServiceSimulator])
}

abstract class TrackServiceSimulatorApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with ClusterComponents
    with AhcWSComponents
    with LagomKafkaClientComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[TrackServiceSimulator](wire[TrackServiceSimulatorImpl])

  override lazy val jsonSerializerRegistry = TrackServiceSimulatorSerializerRegistry

  // Bind the TrackService client
  lazy val trackService: TrackService = serviceClient.implement[TrackService]
}

