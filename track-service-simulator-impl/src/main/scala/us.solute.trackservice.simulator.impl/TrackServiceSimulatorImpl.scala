package us.solute.trackservice.simulator.impl

import java.util.UUID

import com.lightbend.lagom.scaladsl.api.ServiceCall
import org.slf4j.{Logger, LoggerFactory}
import us.solute.trackservice.api.TrackService
import us.solute.trackservice.simulator.api.TrackServiceSimulator
import akka.actor.{ActorRef, ActorSystem, Cancellable, CoordinatedShutdown, Props}
import akka.Done

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
  * Implementation of the TrackServiceSimulator.
  */
class TrackServiceSimulatorImpl(system: ActorSystem, trackService: TrackService)(implicit ec: ExecutionContext) extends TrackServiceSimulator {

  private final val log: Logger =
    LoggerFactory.getLogger(classOf[TrackServiceSimulatorImpl])

  var trackActors: List[ActorRef] = List.empty

  CoordinatedShutdown(system).addTask(CoordinatedShutdown.PhaseServiceStop, "stopTrackSimulationActors") { () =>
    stopAll()
    Future.successful(Done)
  }

  override def startSimulation() = ServiceCall { simulatorProps =>
    stopAll()
    trackActors = (1 to simulatorProps.trackCount).map(i => {
      val trackId = s"track-${i}"
      system.actorOf(Props(new TrackActor(trackService, trackId, simulatorProps.boundingBox,
        simulatorProps.physics, i*50 milliseconds,
        Duration(simulatorProps.period).asInstanceOf[FiniteDuration],
        Duration(simulatorProps.duration).asInstanceOf[FiniteDuration])))

    }).toList
    Future.successful(Done)
  }

  override def stopSimulation() = ServiceCall { _ =>
    stopAll();
    Future.successful(Done)
  }

  def stopAll(): Unit = {
    trackActors.foreach(system.stop)
    trackActors = List.empty
  }

}
